import  numpy as np
cimport numpy as np

#@cython.boundscheck(False)  # turn off array bounds check
#@cython.wraparound(False)   # turn off negative indices ([-1,-1])
#import torch
#model = torch.hub.load('./', 'custom', path='output/best.pt', source='local', force_reload=True)

#cdef np.ndarray[np.uint32_t, ndim=3, mode = 'c'] np_buff = np.ascontiguousarray(im, dtype = np.uint32)
#cdef unsigned int* im_buff = <unsigned int*> np_buff.data

cimport cython
import cython
#import easyocr
#reader = easyocr.Reader(['en'], gpu=True)
cdef list unique_labels = ['Reducer','Weld','Feature_ID','Pipe','Weldolet','Valve','Nozzle','Bend','Flange','Tee','Cap','Coupling','d']
import cv2
import itertools
from pylsd.lsd import lsd
cdef int split_height=320, split_width=320
#cdef float model.conf = 0.08, model.iou = 0.45  # NMS IoU threshold (0-1)

cpdef start_points(int size, int split_size, float overlap=0):
    cdef list points = [0]
    cdef float stride=int(split_size * (1 - overlap))
    cdef float pt
    cdef int counter = 1
    
    while True:
        pt = stride * counter
        if pt + split_size >= size:
            points.append(size - split_size)
            break
        else:
            points.append(pt)
        counter += 1
    return points
    
cpdef find_edges_with_points(np.ndarray[np.uint8_t, ndim=3] img, lsd):
  cdef np.ndarray[np.uint8_t, ndim=3] gray
  cdef list points, X_points, Y_points
  cdef np.ndarray lines
  cdef float x1, y1, x2, y2
  cdef int img_h, img_w
  
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  lines = lsd(gray)#, ang_th=30, eps=8, quant=0.1)
  points = []
  for x1, y1, x2, y2, _ in lines:
      points.append(((x1 + 0.0, y1 + 0.0), (x2 + 0.0, y2 + 0.0)))
          
  img_h = img.shape[0]
  img_w = img.shape[1]
  X_points = start_points(img_w, split_width, 0.60)
  Y_points = start_points(img_h, split_height, 0.60)

  return points, X_points, Y_points
  
cpdef find_bbox_FC_conf_all(list Y_points, list X_points, np.ndarray[np.uint8_t, ndim=3] img, model):
  cdef int index=0, i, j
  cdef list bbox_all = []
  cdef list feature_class_all = []
  cdef list confidence_all = []
  
  cdef np.ndarray[np.uint8_t, ndim=3] split  
  cdef np.ndarray[np.float32_t, ndim=1] feature_class, x, confidence
  #cdef np.ndarray[np.float32_t, ndim=1] confidence
  cdef np.ndarray[np.float32_t, ndim=2] bbox

  #cdef models.common.Detections result_from_img

  cdef bint cond1=True
  for i in Y_points:
      for j in X_points:
          split = img[i:i + split_height, j:j + split_width]
          result_from_img = model(split, size=320)
          feature_class, confidence, bbox = result_from_img.xyxy[0][:, -1].cpu().numpy(), result_from_img.xyxy[0][:, -2].cpu().numpy(), result_from_img.xyxy[0][:, :-2].cpu().numpy()

          bbox[..., 0::2] += j
          bbox[..., 1::2] += i

          #cond1 = next((True for elem in bbox_all if elem is bbox), False)
          #if cond1 == False:
          for x in bbox_all:
            if np.array_equal(bbox, x) == False:
            #if any(np.array_equal(bbox, x) for x in bbox_all) == False:
              bbox_all.append(bbox)
              feature_class_all.append(feature_class)
              confidence = confidence * 100
              confidence_all.append(confidence)
              #print("Labels:", feature_class, "\tConfidence:", confidence, "\tCo-ordinates: ", bbox)
              index += 1
  return bbox_all, feature_class_all, confidence_all
cpdef find_bbox_FC_conf_FID_and_FType(list bbox_all, list feature_class_all, list confidence_all):
  cdef list bbox_all_for_ftype = []
  cdef list feature_class_all_for_ftype = []
  cdef list confidence_all_for_ftype = []

  cdef list bbox_all_FID = []
  cdef list feature_class_all_FID = []
  cdef list confidence_all_FID = []

  cdef np.ndarray[np.float32_t, ndim=2] b
  cdef np.ndarray[np.float32_t, ndim=1] p,c
  cdef int i
  cdef np.float32_t x
  for b, p, c in zip(bbox_all, feature_class_all, confidence_all):
      # if 1 in p:
      for i, x in enumerate(p):
          if x == 2:
              for s in bbox_all_FID:
                if b[np.where(x == p)] in s == False:
                  #if any(b[np.where(x == p)] in s for s in bbox_all_FID) == False:
                      bbox_all_FID.append(b[np.where(x == p)])
                      feature_class_all_FID.append(p[np.where(x == p)])
                      confidence_all_FID.append(c[np.where(x == p)])
          else:
            for s in bbox_all_for_ftype:
                if b in s == False:
                #if any(b in s for s in bbox_all_for_ftype) == False:
                  bbox_all_for_ftype.append(b)
                  feature_class_all_for_ftype.append(p)
                  confidence_all_for_ftype.append(c)
  return bbox_all_FID, feature_class_all_FID, confidence_all_FID, bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype
cpdef point_inside_bbox2(float x1, float y1, float x2, float y2, float x, float y):
    if (x > x1 and x < x2 and
            y > y1 and y < y2):
        return True
    else:
        return False
        
cpdef find_FIDtext_and_its_bbox(list points, list bbox_all_FID, np.ndarray[np.uint8_t, ndim=3] img, reader):
  cdef list pnt_coord = []
  cdef list bnd_coord = []
  cdef list pnt_bnd_pair = []

  cdef list pnt_coord_for_ftype = []
  cdef list bnd_coord_for_ftype = []
  cdef list pnt_bnd_pair_for_ftype = []

  cdef list fid_text = []
  cdef list fid_conf = []
  #images_grouped = []
  #for pnt in points:
      #print(pnt)
  #    for p in pnt:
  cdef np.ndarray[np.uint8_t, ndim=2] vertical_kernel,horizontal_kernel
  cdef np.ndarray[np.uint8_t, ndim=3] clip, remove_vertical,remove_horizontal
  cdef list data1
  for p in itertools.chain(*points):
          if p not in pnt_coord:
              for b in itertools.chain(*bbox_all_FID):
              #for bnd in bbox_all_FID:
              #    for b in bnd:
                      if point_inside_bbox2(b[0], b[1], b[2], b[3], p[0], p[1]):
                          clip = img[int(b[1]):int(b[3]), int(b[0]):int(b[2])]
                          # clip = rotate_image(clip,15)
                          vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 50))
                          remove_vertical = 255 - cv2.morphologyEx(clip, cv2.MORPH_CLOSE, vertical_kernel)

                          horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (50, 1))
                          remove_horizontal = 255 - cv2.morphologyEx(clip, cv2.MORPH_CLOSE, horizontal_kernel)

                          clip = cv2.add(cv2.add(remove_vertical, remove_horizontal), clip)
                          clip = cv2.resize(clip, None, fx=3, fy=2, interpolation=cv2.INTER_CUBIC)
                          #images_grouped.append(clip)
                          data1 = reader.readtext(clip)

                          try:
                              fid_text.append(data1[0][1])
                              fid_conf.append(round(data1[0][2], 3) * 100)
                              pnt_coord.append(p)
                              bnd_coord.append(b)
                              pnt_bnd_pair.append([p, b])
                          except:
                              pass
  #print(reader.readtext_batched(images_grouped))
  return fid_text, fid_conf, pnt_bnd_pair                            

cpdef deep_index(list lst, w):
    return [(i, sub.index(w)) for (i, sub) in enumerate(lst) if w in sub]

cpdef pair_FID_FType(list fid_text, list fid_conf, list pnt_bnd_pair, list points, list bbox_all_for_ftype, list feature_class_all_for_ftype, list confidence_all_for_ftype):
  cdef list line_point_pair = []
  # line_point_pair2 = []
  cdef float fc_FT, conf_all, f_conf
  cdef np.ndarray b_a_ft
  cdef list fid_list = []
  cdef list f_conf_list = []
  cdef list ftype_list = []
  cdef list fid_ftype_pair = []
  cdef list conf_all_list = []
  cdef str fid_TXT
  cdef list centroid_FT = []
  cdef list line_end_pt_on_FT = []
  cdef int index_for_bbox = 0
  cdef list point_match_index
  for fid_TXT, f_conf, point in zip(fid_text, fid_conf, pnt_bnd_pair):
      point_match_index = deep_index(points, point[0])
      for bnf_for_ftype, feat_c_FT, confidence_all in zip(bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype):
          for b_a_ft, fc_FT, conf_all in zip(bnf_for_ftype, feat_c_FT, confidence_all):
      #for b_a_ft, fc_FT, conf_all in zip(itertools.chain(bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype)):
            if (point_match_index[0][1] == 0):
              if point_inside_bbox2(b_a_ft[0],b_a_ft[1],b_a_ft[2],b_a_ft[3],points[point_match_index[0][0]][1][0],points[point_match_index[0][0]][1][1]):
                  if unique_labels[int(fc_FT)] != 'Feature_ID':
                      centroid_XY_points = (center_x_point_of_rect, center_y_point_of_rect)
                      centroid_FT.append(centroid_XY_points)
                      line_end_pt_on_FT.append(points[point_match_index[0][0]][1])

                      fid_list.append(fid_TXT)
                      f_conf_list.append(f_conf)
                      ftype_list.append(unique_labels[int(fc_FT)])
                      conf_all_list.append(conf_all)
            if (point_match_index[0][1] == 1):
              if point_inside_bbox2(b_a_ft[0],b_a_ft[1],b_a_ft[2],b_a_ft[3],points[point_match_index[0][0]][0][0],points[point_match_index[0][0]][0][1]):
                if unique_labels[int(fc_FT)] != 'Feature_ID':
                    center_x_point_of_rect = (b_a_ft[0] + b_a_ft[2]) / 2
                    center_y_point_of_rect = (b_a_ft[1] + b_a_ft[3]) / 2
                    centroid_XY_points = (center_x_point_of_rect, center_y_point_of_rect)
                    centroid_FT.append(centroid_XY_points)
                    line_end_pt_on_FT.append(points[point_match_index[0][0]][0])

                    fid_list.append(fid_TXT)
                    f_conf_list.append(f_conf)
                    ftype_list.append(unique_labels[int(fc_FT)])
                    conf_all_list.append(conf_all)
      index_for_bbox += 1
  return centroid_FT, line_end_pt_on_FT, fid_list, f_conf_list, ftype_list, conf_all_list  
