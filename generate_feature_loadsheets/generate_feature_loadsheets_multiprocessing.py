
import time
import sys

sys.path.insert(1, '$HOME/tmp/')
sys.path.append('$HOME/tmp/')

import torch, torchvision
import cv2
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv
from pylsd.lsd import lsd

import pytesseract
from pdf2image import convert_from_path

from pdf2image import convert_from_path
import glob, os, shutil
from os import listdir

import xlsxwriter
import easyocr
import os
import json
import torch

import warnings
warnings.filterwarnings("ignore")
import cv2
import random
import matplotlib.pyplot as plt
import os
import re
import pytesseract
import cv2
import random
import pytesseract
from pdf2image import convert_from_path

from pdf2image import convert_from_path
import glob, os, shutil
from os import listdir
import math

from itertools import groupby
from operator import itemgetter

import natsort
import itertools
import cython
#reader = easyocr.Reader(['en'])
reader = easyocr.Reader(['en'], gpu=True)

extension1 = ('.pdf')
extension2 = ('.png')
extension3 = ('.txt')
pytesseract.pytesseract.tesseract_cmd = (r'/usr/local/bin/tesseract')
#pytesseract.pytesseract.tesseract_cmd = (r'/usr/bin/tesseract')

def convert_pdf_to_image(rootdir):
  for subdir, dirs, files in os.walk(rootdir):
      #print("subdir, dirs, files",type(subdir),type(dirs), type(files))
      for file in files:
          #print("file:", type(file))
          ext = os.path.splitext(file)[-1].lower()
          #print("ext,ext1",type(ext),type(extension1))
          if ext in extension1:
              # print ("\nProcessing file ",os.path.join(subdir, file))
              pages = convert_from_path(os.path.join(subdir, file),
                                        250)  # 400 is the Image quality in DPI (highest 800, default 200)
              # print(pages)
              #print("pages", type(pages))
              for i in range(len(pages)):
                  # print("saving page ", i+1, "as an image:")
                  pages[i].save(os.path.join(subdir, os.path.splitext(file)[0]) + "_" + str(i + 1) + ".png")

def remove_images_converted(rootdir):
  for subdir, dirs, files in os.walk(rootdir):
      for file in files:
          ext = os.path.splitext(file)[-1].lower()
          if ext in extension2:
              os.remove(os.path.join(subdir, file))

def nextword(target, source):
    #print("target, source",type(target), type(source))
    for i, w in enumerate(source):
        #print("i,w", type(i),type(w))
        if w == target:
            return source[i + 1]

def nextword_JV_Project_drawing(value, items):
    try:
        
        i = items.index(value)
        #print("value,items,i",type(value),type(items),type(i))
        return items[i + 2]
    except ValueError:
        pass
        # i = items.index(value)
        # return items[i+2]

def prevword_JV_Project_piping(value, items):
    # print(items)
    # print(str(s for s in items if value in s))
    for i, s in enumerate(items):
        #print("value,items,i,s",type(value),type(items),type(i),type(s))
        if value in s:
            return items[i - 2]
    # i = items.index(int([i for i, s in enumerate(items) if value in s]))
    # return items[i-2]

def cleanup_text(text):
    # strip out non-ASCII text so we can draw the text on the image
    # using OpenCV
    return "".join([c if ord(c) < 128 else "" for c in text]).strip()


def start_points(size, split_size, overlap=0):
    points = [0]
    stride = int(split_size * (1 - overlap))
    counter = 1
    #print("size, split_size, overlap, points,stride,counter", type(size), type(split_size), type(overlap),type(points),type(stride),type(counter))
    while True:
        pt = stride * counter
        #print("pt",type(pt))
        if pt + split_size >= size:
            points.append(size - split_size)
            break
        else:
            points.append(pt)
        counter += 1
    return points

"""
def point_inside_bbox2(x1, y1, x2, y2, x, y):
    if (x > x1 and x < x2 and
            y > y1 and y < y2):
        return True
    else:
        return False
"""
# list is a subset of another nested list
def checkSubset(list1, list2):
    temp1 = []
    temp2 = []
    for i in list1:
        temp1.append(tuple(i))
    for i in list2:
        temp2.append(tuple(i))

    return set(temp2) < set(temp1)

#from google.colab.patches import cv2_imshow

def distance(p1, p2):
    dist = math.sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2)
    #print("p1,p2,dist",type(p1),type(p2),type(dist))
    return dist

def closest(pt, others, feature_list):
    closest_pt = min(others, key=lambda i: distance(pt, i))
    # print("Closest point: ", closest_pt)
    index = others.index(closest_pt)
    # print("Index of closest point: ", index)
    closest_FT = feature_list[index]
    return closest_FT

def find_mean(test_list):
    res = [sum(ele) / len(test_list) for ele in zip(*test_list)]
    return res

def convert_list_to_tuple(list):
    return tuple(list)

def atoi(text):
    #print(text)
    return int(text) if text.isdigit() else text

def natural_keys(text):

    #print([ atoi(c) for c in re.split(r'(\d+)', text) ])
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]

def get_prefix(l, m):
    if not l: return []
    if m is not None: l.sort()
    r = [(k + p, f or len(g)) for k, g in [(k, list(g)) for k, g in groupby(l, itemgetter(0))] if len(g) > 1 for p, f in get_prefix([s[1:] for s in g if len(s) > 1], None)] + [('', 0)]
    if m: return sorted([(p, f) for p, f in r if len(p) >= m], key=itemgetter(1), reverse=True)
    return r
    
def remove_duplicates(item_list):
    singles_list = []
    #print("item_list,singles_list",type(item_list),type(singles_list))
    for element in item_list:
        #print("element",type(element))
        if element not in singles_list:
            singles_list.append(element)
    return singles_list


model = torch.hub.load('/var/www/html/veracity/generate_feature_loadsheets_multiprocessing/', 'custom', path= '/var/www/html/veracity/generate_feature_loadsheets_multiprocessing/output/best.pt', source='local', force_reload=True)
#model = torch.hub.load('./', 'custom', path='output/best.pt', source='local', force_reload=True)
model.conf = 0.08  # confidence threshold (0-1)
model.iou = 0.45  # NMS IoU threshold (0-1)
#model.nms = 0.25
split_width = 320
split_height = 320

import pyximport
pyximport.install()
#import find_bbox_FC_conf_of_all
#import find_bbox_FC_conf_of_FID_and_FType
import cythonize_modules_to_c
#@ray.remote(num_returns=3)
def find_edges_with_points(img):
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  lines = lsd(gray)#, ang_th=30, eps=8, quant=0.1)
  points = []
  for x1, y1, x2, y2, _ in lines:
      points.append(((x1 + 0.0, y1 + 0.0), (x2 + 0.0, y2 + 0.0)))
          
  img_h, img_w, _ = img.shape
  X_points = start_points(img_w, split_width, 0.60)
  Y_points = start_points(img_h, split_height, 0.60)
  return points, X_points, Y_points

#model = ray.put(model)
#@ray.remote(num_returns=3)


def find_bbox_FC_conf_all(Y_points, X_points, img):
  index = 0
  bbox_all = []
  feature_class_all = []
  confidence_all = []

  #print("img",type(img))
  for i in Y_points:
      for j in X_points:
          split = img[i:i + split_height, j:j + split_width]
          #print("split",type(split),"ndim", split.ndim)
          result_from_img = model(split, size=320)
          #print("result_from_img",type(result_from_img))
          feature_class, confidence, bbox = result_from_img.xyxy[0][:, -1].cpu().numpy(), \
                                +            result_from_img.xyxy[0][:, -2].cpu().numpy(), \
                                            result_from_img.xyxy[0][:, :-2].cpu().numpy()
          #print("feature_class",type(feature_class),"ndim", feature_class.ndim)
          #print("confidence",type(confidence),"ndim", confidence.ndim)
          #print("bbox",type(bbox),"ndim", bbox.ndim)
          bbox[..., 0::2] += j
          bbox[..., 1::2] += i
          cond1 = next((True for elem in bbox_all if elem is bbox), False)
          #print("cond1",type(cond1))
          if cond1 == False:
              bbox_all.append(bbox)
              feature_class_all.append(feature_class)
              confidence = confidence * 100
              confidence_all.append(confidence)
              #print("Labels:", feature_class, "\tConfidence:", confidence, "\tCo-ordinates: ", bbox)
              index += 1
  return bbox_all, feature_class_all, confidence_all


def find_bbox_FC_conf_FID_and_FType(bbox_all, feature_class_all, confidence_all):
  bbox_all_for_ftype = []
  feature_class_all_for_ftype = []
  confidence_all_for_ftype = []

  bbox_all_FID = []
  feature_class_all_FID = []
  confidence_all_FID = []

  for b, p, c in zip(bbox_all, feature_class_all, confidence_all):
      # if 1 in p:
      #print("b,p,c",type(b),type(p),type(c), b.ndim, p.ndim, c.ndim)
      for i, x in enumerate(p):
          #print("i,x", type(i), type(x), x.ndim)
          if x == 2:
              if any(b[np.where(x == p)] in s for s in bbox_all_FID) == False:
                  #print("s",type(s))
                  bbox_all_FID.append(b[np.where(x == p)])
                  feature_class_all_FID.append(p[np.where(x == p)])
                  confidence_all_FID.append(c[np.where(x == p)])
          else:
              if any(b in s for s in bbox_all_for_ftype) == False:
                  #print("s", type(s))
                  bbox_all_for_ftype.append(b)
                  feature_class_all_for_ftype.append(p)
                  confidence_all_for_ftype.append(c)
  return bbox_all_FID, feature_class_all_FID, confidence_all_FID, bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype

def construct_features_dataframe(ftype_list, conf_all_list, fid_list, f_conf_list, centroid_FT, line_end_pt_on_FT, fid_text):
  #print("Number of feature IDs paired: ", len(set(fid_list)))
  fid_ftype_df = pd.DataFrame(
      list(zip(ftype_list, conf_all_list, fid_list, f_conf_list, centroid_FT, line_end_pt_on_FT)),
      columns=['Feature_Type', 'Feature Type Confidence', 'Feature_ID', 'Feature ID Confidence',
                'Centroid_FT', 'Line_point_on_FT'])
  fid_ftype_df = fid_ftype_df.groupby(['Feature_ID'], as_index=False).agg(lambda x: x.tolist())    
  fid_ftype_df['Line_point_on_FT_Avg'] = fid_ftype_df.apply(lambda row: find_mean(row.Line_point_on_FT), axis=1)
  fid_ftype_df['Line_point_on_FT_Avg'] = fid_ftype_df.apply(lambda row: convert_list_to_tuple(row.Line_point_on_FT_Avg), axis=1)
  fid_ftype_df['Closest_FT'] = fid_ftype_df.apply(lambda row: closest(row.Line_point_on_FT_Avg, row.Centroid_FT, row.Feature_Type), axis=1)

  try:
      fid_ftype_df["Feature ID Confidence"] = fid_ftype_df["Feature ID Confidence"].str[0]
      fid_ftype_df["Feature Type Confidence"] = fid_ftype_df["Feature Type Confidence"].str[0]
  except:
      pass
  fid_unidentified = np.setdiff1d(fid_text, fid_list)
  fid_unidentified = fid_unidentified.tolist()
  fid_unidentified = pd.DataFrame(fid_unidentified, columns=['Feature_ID'])  # .transpose()
  fid_ftype_df = fid_ftype_df.append(fid_unidentified, ignore_index=True)
  fid_ftype_df = fid_ftype_df.replace(np.nan, '', regex=True)
  for index, row in fid_ftype_df["Feature_Type"].iteritems():
      ft_string = ", ".join(row)
      fid_ftype_df.loc[index, "Feature_Type"] = ft_string
  fid_ftype_df.rename(columns={'Feature_Type': 'Feature Type'}, inplace=True)
  # print(fid_ftype_df)
  fid_ftype_df.rename(columns={'Feature_ID': 'Feature ID'}, inplace=True)
  #fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('_', '').str.replace('S', '5').str.replace('s', '5').str.replace('T', '1').str.replace('t', '1').str.replace('A', '4')('o', '0').str.replace('O', '0').str.replace('c', '0').str.replace('C', '0').str.replace(' ', '').str.replace('I', '1').str.replace('|', '1').str.replace('l', '1').str.replace('\[', '1').str.replace('\]', '1').str.replace('\(', '1').str.replace('\)', '1').str.replace('z', '2').str.replace('Z', '2').str.replace('g', '9').str.replace('^', '1').str.replace('?', '7').str.replace('-', '').str.replace('@', '0')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('_', '')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('S', '5')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('s', '5')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('T', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('t', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('A', '4')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('o', '0')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('O', '0')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('c', '0')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('C', '0')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace(' ', '')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('I', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('|', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('l', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('\[', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('\]', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('\(', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('\)', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('z', '2')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('Z', '2')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('g', '9')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('^', '1')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('?', '7')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('-', '')
  fid_ftype_df['Feature ID'] = fid_ftype_df['Feature ID'].str.replace('@', '0')
  fid_ftype_df.drop(columns=['Feature Type', 'Centroid_FT', 'Line_point_on_FT', 'Line_point_on_FT_Avg'],
                    inplace=True)
  fid_ftype_df.rename(columns={'Closest_FT': 'Feature Type'}, inplace=True)
  fid_ftype_df = fid_ftype_df[['Feature ID', 'Feature Type', 'Feature Type Confidence', 'Feature ID Confidence']]
  #print("fid_ftype_df:",fid_ftype_df)
  # fid_ftype_df.to_csv('fid_ftype_pair_output.csv',index=False)
  return fid_ftype_df

def fill_missing_FIDs(fid_ftype_df):
  list_of_FID = fid_ftype_df['Feature ID'].tolist()
  list_of_FID.sort(key=natural_keys)
  most_frequent_character_prefix = ''
  try:
      most_frequent_character_prefix = get_prefix(list_of_FID, 1)[0][0]
  except:
      pass
  
  numeric_ids = [re.sub("[^0-9.]", "", s) for s in list_of_FID]
  #numeric_ids = [re.sub("[^0-9,.]", "", s) for s in list_of_FID]
  numeric_ids = list(filter(None, numeric_ids)) # remove empty strings
  numeric_ids = [float(x) for x in numeric_ids] #convert list to int
  numeric_ids.sort()
  
  range_list_to_fill=[]

  #print("fid_ftype_df, list_of_FID, most_frequent_character_prefix, numeric_ids", type(fid_ftype_df), type(list_of_FID), type(most_frequent_character_prefix), type(numeric_ids))
  for idx, a in enumerate(numeric_ids):
      #print("a",type(a))
      if numeric_ids[idx:idx+3] == [a,a+1,a+2]:
          #print([a, a+1,a+2])
          range_list_to_fill.append(a+2)
          break
  
  numeric_ids_reverse = numeric_ids[::-1]
  #print(numeric_ids_reverse)
  for idx, a in enumerate(numeric_ids_reverse):
      if numeric_ids_reverse[idx:idx+3] == [a,a-1,a-2]:
          #print([a, a-1,a-2])
          range_list_to_fill.append(a-2)
          break
  
  try:
      #fill values in range
      FIDs_to_fill = [*range(int(range_list_to_fill[0])+1, int(range_list_to_fill[1]), 1)]
      
      #check if "most_frequent_character_prefix" is character. If so append it to every element. Else keep the list as is
      if most_frequent_character_prefix.isalpha():
        FIDs_to_fill = [most_frequent_character_prefix + str(item) for item in FIDs_to_fill]
      
      FIDs_to_fill = [str(x) for x in FIDs_to_fill]

      #find elements present in the new list and not in the old list, to find FIDs not recognized by AI model
      missing_FIDs = list(set(FIDs_to_fill) - set(list_of_FID))
      
      missing_FIDs_df=pd.DataFrame(missing_FIDs,columns=['Feature ID'])
      fid_ftype_df = fid_ftype_df.append(missing_FIDs_df, ignore_index=True)
  except:
      pass
  return fid_ftype_df

def enhance_image_quality(image):
  gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]

  # Remove horizontal lines
  horizontal_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (15, 1))
  detect_horizontal = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, horizontal_kernel, iterations=2)
  cnts = cv2.findContours(detect_horizontal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
  cnts = cnts[0] if len(cnts) == 2 else cnts[1]
  for c in cnts:
      cv2.drawContours(thresh, [c], -1, (0, 0, 0), 2)

  # Remove vertical lines
  vertical_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 15))
  detect_vertical = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, vertical_kernel, iterations=2)
  cnts = cv2.findContours(detect_vertical, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
  cnts = cnts[0] if len(cnts) == 2 else cnts[1]
  for c in cnts:
      cv2.drawContours(thresh, [c], -1, (0, 0, 0), 3)

  # Dilate to connect text and remove dots
  kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (11, 1))
  dilate = cv2.dilate(thresh, kernel, iterations=2)
  cnts = cv2.findContours(dilate, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
  cnts = cnts[0] if len(cnts) == 2 else cnts[1]
  for c in cnts:
      area = cv2.contourArea(c)
      if area < 500:
          cv2.drawContours(dilate, [c], -1, (0, 0, 0), -1)

  # Bitwise-and to reconstruct image
  result = cv2.bitwise_and(image, image, mask=dilate)
  result[dilate == 0] = (255, 255, 255)
  return result

def find_line_draw_num(img):
  y, x = 0, 0
  h, w, _ = img.shape
  # crop bottom 20 percent of image
  #image=img.copy()
  image = img[int(h * 0.7):y + h, int(w * 0.65):x + w]
  # OCR
  data = pytesseract.image_to_string(image, lang='eng', config='--psm 6')
  #data = pytesseract.image_to_string(result, lang='eng', config='--psm 6')
  line_num = None
  draw_num = None
  data = data.splitlines()
  for index, line in enumerate(data):
      # if "Drawing" in line:
      try:
          if (re.search("DRAWING N", line)):
              line_draw_no = data[index + 1]
              line_num = line_draw_no.split()[0]
              draw_num = line_draw_no.split()[1]
      except:
          pass

  if line_num is None or draw_num is None:
      #image = cv2.imread(os.path.join(subdir, file), cv2.IMREAD_COLOR)
      #image = img.copy()
      y, x = 0, 0
      h, w, _ = img.shape
      # crop bottom 20 percent of image
      image = img[int(h * 0.8):y + h, x:x + w]
      result = enhance_image_quality(image)
      # OCR
      data = pytesseract.image_to_string(result, lang='eng', config='--psm 6')
      try:
          line_num = nextword('TITLE:', data.split())
          draw_num = nextword('NTS', data.split())
      except:
          pass
      for word in data.split():
          ln_no = word
          dr_no = word
          try:
              if line_num is None:
                  if ln_no.count('-') > 2 and ln_no.count('"') > 0:
                      line_num = ln_no

              if draw_num is None:
                  if dr_no.count('-') > 2 and (dr_no.count('ISO') > 0):
                      draw_num = dr_no
          except:
              pass
  if line_num is None or draw_num is None:
      text_detected = []
      #image = cv2.imread(os.path.join(subdir, file), cv2.IMREAD_COLOR)
      #image = img.copy()
      y, x = 0, 0
      h, w, _ = img.shape
      # crop bottom 20 percent of image
      image = img[int(h * 0.8):y + h, x:x + w]
      data1 = reader.readtext(image)
      for (bbox, text, prob) in data1:
          text = cleanup_text(text)
          text_detected.append(text)
      try:
          draw_num = nextword_JV_Project_drawing('GALVANIZED', text_detected)
          line_num = prevword_JV_Project_piping('STRESS', text_detected)
      except:
          pass
      # print(pipe_num)
      try:
          if line_num is None:
              for i, s in enumerate(text_detected):
                  if ("\"-A" in s) or ("\'-A" in s):
                      line_num = text_detected
      except:
          pass

      if draw_num is None:
          draw_num = nextword('Approved', text_detected) #prevword_JV_Project_piping('IN SALAH GAS PROJECT', text_detected)

      if draw_num is None:
          draw_num = nextword_JV_Project_drawing('Sheet', text_detected) #prevword_JV_Project_piping('IN SALAH GAS PROJECT', text_detected)
          if draw_num=="Date":
            draw_num=None

      if draw_num is None:
          draw_num = nextword_JV_Project_drawing('Sheet', text_detected) #prevword_JV_Project_piping('IN SALAH GAS PROJECT', text_detected)
          if draw_num=="PAGE":
            draw_num=None
  return line_num, draw_num


from os.path import basename
def process_isometrics_using_gpu(file, subdir, rootdir):
  #print("\nProcessing file ", os.path.splitext(file)[0] + '.pdf\n')
  
  start_iso = time.time()
  #img = cv2.imread(os.path.join(subdir, file))
  img = cv2.imread(file)
  #print(img)
  #points, X_points, Y_points = cythonize_modules_to_c.find_edges_with_points(img, lsd)
  points, X_points, Y_points = find_edges_with_points(img)
  #pts = find_edges_with_points.remote(img)
  #points, X_points, Y_points = ray.get(pts)
  bbox_all, feature_class_all, confidence_all = find_bbox_FC_conf_all(Y_points, X_points, img)
  #bbox_all, feature_class_all, confidence_all = cythonize_modules_to_c.find_bbox_FC_conf_all(Y_points, X_points, img, model)
  #bbox_ft_conf_all = find_bbox_FC_conf_all.remote(Y_points, X_points, img)
  #bbox_all, feature_class_all, confidence_all = ray.get(bbox_ft_conf_all)

  #bbox_all_FID, feature_class_all_FID, confidence_all_FID, bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype = cythonize_modules_to_c.find_bbox_FC_conf_FID_and_FType(bbox_all, feature_class_all, confidence_all)
  bbox_all_FID, feature_class_all_FID, confidence_all_FID, bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype = find_bbox_FC_conf_FID_and_FType(bbox_all, feature_class_all, confidence_all)
  fid_text, fid_conf, pnt_bnd_pair = cythonize_modules_to_c.find_FIDtext_and_its_bbox(points, bbox_all_FID, img, reader)
  #fid_text, fid_conf, pnt_bnd_pair = find_FIDtext_and_its_bbox(points, bbox_all_FID, img)
  centroid_FT, line_end_pt_on_FT, fid_list, f_conf_list, ftype_list, conf_all_list = cythonize_modules_to_c.pair_FID_FType(fid_text, fid_conf, pnt_bnd_pair, points, bbox_all_for_ftype, feature_class_all_for_ftype, confidence_all_for_ftype)

  fid_ftype_df = construct_features_dataframe(ftype_list, conf_all_list, fid_list, f_conf_list, centroid_FT, line_end_pt_on_FT, fid_text)
  fid_ftype_df = fill_missing_FIDs(fid_ftype_df)

  fid_ftype_df = fid_ftype_df.iloc[natsort.index_humansorted(fid_ftype_df['Feature ID'])]
  fid_ftype_df = fid_ftype_df[fid_ftype_df['Feature ID'].astype(str).str.isalnum()] #remove non alphanumeric rows with special characters on Feature ID column

  line_num, draw_num = find_line_draw_num(img)

  try:
      f_size = re.findall(r"(\d+).*", line_num)
  except:
      f_size = ''
      pass
  head, xlfile = os.path.split(file)
  print("corrosion circuit", basename(head))
  line_drawing_df = pd.DataFrame([basename(head), line_num, draw_num, f_size]).transpose()
  #line_drawing_df = pd.DataFrame([sys.argv[1].split(':')[0], line_num, draw_num, f_size]).transpose()
  line_drawing_df.columns = ['Corrosion Circuit*', 'Piping Tag/ Line Number*', 'Drawing Number',
                              'Feature Size, inches']
  for index, row in line_drawing_df["Feature Size, inches"].iteritems():
      ft_string = ", ".join(row)
      # print(ft_string)
      line_drawing_df.loc[index, "Feature Size, inches"] = ft_string
  try:
      line_drawing_df = pd.concat([line_drawing_df] * len(fid_ftype_df))
  except:
      line_drawing_df = pd.concat([line_drawing_df] * 1)

  loadsheet = line_drawing_df.reset_index(drop=True).merge(fid_ftype_df.reset_index(drop=True),
                                                            left_index=True, right_index=True)
  loadsheet['Feature Class'] = "TML"
  # loadsheet = pd.concat([line_drawing_df, fid_ftype_df], ignore_index=True)
  loadsheet = loadsheet[
      ['Feature ID Confidence', 'Feature Type Confidence', 'Corrosion Circuit*', 'Piping Tag/ Line Number*',
        'Feature Class', 'Feature Type', 'Feature ID', 'Feature Size, inches',
        'Drawing Number']]  # reordering the columns
  loadsheet = loadsheet.reindex(
      loadsheet.columns.tolist() + ['Status Indicator', 'Design Code', 'Design Code Year', 'Material Type',
                                    'Material Specification', 'Material Grade', 'Design Pressure',
                                    'Design Pressure Unit', 'Design Temperature', 'Design Temperature Unit',
                                    'Allowable Stress', 'Allowable Stress Unit', 'Outer Diameter',
                                    'Outer Diameter Unit', 'Inside Diameter', 'Inside Diameter Unit',
                                    'Joint Factor', 'Corrosion Allowance, mm', 'Insulation Status',
                                    'Schedule', 'NWT, mm', 'Piping Formula', 'Mechanical Allowance',
                                    'Design Factor', 'Temperature Factor', 'CONC(ALARM1), mm',
                                    'MAWT(ALARM2), mm', 'Access'], axis=1)  # version > 0.20.0
  # print(loadsheet.columns)
  loadsheet.columns = pd.MultiIndex.from_product([['Feature Attributes'], loadsheet.columns])
  loadsheet[('Feature Attributes', 'Feature ID Confidence')] = pd.to_numeric(
      loadsheet[('Feature Attributes', 'Feature ID Confidence')])
  loadsheet[('Feature Attributes', 'Feature Type Confidence')] = pd.to_numeric(
      loadsheet[('Feature Attributes', 'Feature Type Confidence')])
  loadsheet.insert(loc=0, column=('Feature Attributes', 'Feature Confidence'), value=(loadsheet[(
  'Feature Attributes', 'Feature ID Confidence')] + loadsheet[(
  'Feature Attributes', 'Feature Type Confidence')]) / 2)

  bins = [0.0, 20.0, 60.0, 100.0]
  labels = ["Low", "Med", "High"]
  loadsheet[('Feature Attributes', 'Feature Confidence')] = pd.cut(
      loadsheet[('Feature Attributes', 'Feature Confidence')], bins=bins, labels=labels)
  loadsheet[('Feature Attributes', 'Feature Confidence')] = loadsheet[
      ('Feature Attributes', 'Feature Confidence')].fillna('Low')

  loadsheet.drop(
      [('Feature Attributes', 'Feature ID Confidence'), ('Feature Attributes', 'Feature Type Confidence')],
      axis=1, inplace=True)

  #print("Generating loadsheet")
  #head, xlfile = os.path.split(file)
  
  try:
      writer = pd.ExcelWriter(head, os.path.splitext(xlfile)[0]+'.xlsx', engine='xlsxwriter')
      sheetname = os.path.join(os.path.splitext(xlfile)[0]+'.xlsx')
  except:
      writer = pd.ExcelWriter(os.path.join(head, os.path.splitext(xlfile)[0][-26:]) + '.xlsx', engine='xlsxwriter')
      sheetname = os.path.join(os.path.splitext(xlfile)[0][-26:] + '.xlsx')
  loadsheet.to_excel(writer, sheet_name=sheetname)  # send df to writer
  worksheet = writer.sheets[sheetname]  # pull worksheet object
  for idx, col in enumerate(loadsheet):  # loop through all columns
      series = loadsheet[col]
      max_len = max((
          series.astype(str).map(len).max(),  # len of largest item
          len(str(series.name))  # len of column name/header
      )) + 1  # adding a little extra space
      worksheet.set_column(idx, idx, max_len)  # set column width
  writer.save()
  end_iso = time.time()
  #print("Time taken to process the isometric", os.path.join(subdir, file), ": ", end_iso - start_iso)
  #return loadsheet
#except:
  #pass

import multiprocessing
#from multiprocessing import Pool
from torch.multiprocessing import Pool, Process, set_start_method

import itertools
import requests
#from pathos.multiprocessing import ProcessingPool as Pool
status="successfully"
code="500"
transaction_id = ''
import argparse
 
if __name__ == '__main__':
  try:
    #parser = argparse.ArgumentParser()
    #parser.add_argument("-i", "--Input", help = "Show Input")
    #args = parser.parse_args()
    baseSystemPath = "/var/www/html/veracity/generate_feature_loadsheets_multiprocessing/corrision_circuit/"
    user_input = baseSystemPath+sys.argv[1] #"corrision__VTID3456758133" # sys.argv[1]# #"isometrics_VTID8959539612" #args.Input #"isometrics_VTID8959539612" #sys.argv[1]#input("Enter the directory path where PDF files are saved: \n")
    #user_input = os.path.join(baseSystemPath, sys.argv[1])
    #print(user_input)
    # user_input = './'
    assert os.path.exists(user_input), "Path not found, " + str(user_input)
    rootdir = user_input
    #print(parser)

    transaction_id = sys.argv[1] 
    transaction_id=transaction_id.split('__')[1]
    #transaction_id="isometrics__VTID8667604542".split('__')[1]

    #transaction_id = "VTID3456758133"
    print(transaction_id)
    convert_pdf_to_image(rootdir)
    start = time.time()
    f = open(os.path.join(rootdir, 'processed_files.txt'), 'w')
    f.write('Processed isometrics within the directory ' + sys.argv[1].split('__')[0] + ' are:\n\n')
    
    file_list=[]
    for subdir, dirs, files in os.walk(rootdir):
      for file in files:
        ext = os.path.splitext(file)[-1].lower()
        if ext in extension2:
          input_files = os.path.join(subdir, file)
          file_list.append(input_files)
    #print(file_list)
    
    try:
      set_start_method('spawn')
    except RuntimeError:
      #except:
      pass
    pool = Pool(processes = multiprocessing.cpu_count())                         # Create a multiprocessing Pool
    #pool = Pool()
    #try:
    #torch.multiprocessing.set_start_method('spawn')
    #except:
    #  multiprocessing.set_start_method('spawn', force=True)
    #multiprocessing.set_start_method('forkserver', force=True)
    #print(torch.multiprocessing.get_start_method())
    #for file in file_list:
      #pool.apply_async(process_isometrics_using_gpu, [file])
      #pool.map(process_isometrics_using_gpu, [file])
    pool.starmap(process_isometrics_using_gpu, zip(file_list, itertools.repeat(subdir), itertools.repeat(rootdir)))
    #pool.map(process_isometrics_using_gpu, zip(file_list, itertools.repeat(subdir), itertools.repeat(rootdir)))
    #pool.map_async(process_isometrics_using_gpu, file_list)
    #pool.apply(process_isometrics_using_gpu, file_list)
    #pool.imap_unordered(process_isometrics_using_gpu, file_list)
    #pool.imap(process_isometrics_using_gpu, file_list)
    
    #loadsheet = pool.map(process_isometrics_using_gpu, file_list)
    #print(loadsheet)
    pool.close()
    pool.join()
    
    
    
    
    remove_images_converted(rootdir)
    df = pd.DataFrame()
    processed_file_list=[]
    for subdir, dirs, files in os.walk(rootdir):
        for file in files:
            if file.endswith('.xlsx'):
                # print(file,len(file))
                df = df.append(pd.read_excel(os.path.join(subdir, file), header=None, skiprows=[0, 1], index_col=0),
                               ignore_index=True)
                processed_file_list.append(os.path.splitext(file)[0].split('_')[0]+'.pdf\n')
                # df.drop(df.tail(1).index,inplace=True) # drop last 1 row(s)
    # print(df)
    processed_file_list = "\n".join(processed_file_list)
    f.write(processed_file_list)
    f.close()
    df = df.dropna(how='all')
    
    df.columns = pd.MultiIndex.from_tuples([('Feature Attributes', 'Feature Confidence'),
                                            ('Feature Attributes', 'Corrosion Circuit*'),
                                            ('Feature Attributes', 'Piping Tag/ Line Number*'),
                                            ('Feature Attributes', 'Feature Class'),
                                            ('Feature Attributes', 'Feature Type'),
                                            ('Feature Attributes', 'Feature ID'),
                                            ('Feature Attributes', 'Feature Size, inches'),
                                            ('Feature Attributes', 'Drawing Number'),
                                            ('Feature Attributes', 'Status Indicator'),
                                            ('Feature Attributes', 'Design Code'),
                                            ('Feature Attributes', 'Design Code Year'),
                                            ('Feature Attributes', 'Material Type'),
                                            ('Feature Attributes', 'Material Specification'),
                                            ('Feature Attributes', 'Material Grade'),
                                            ('Feature Attributes', 'Design Pressure'),
                                            ('Feature Attributes', 'Design Pressure Unit'),
                                            ('Feature Attributes', 'Design Temperature'),
                                            ('Feature Attributes', 'Design Temperature Unit'),
                                            ('Feature Attributes', 'Allowable Stress'),
                                            ('Feature Attributes', 'Allowable Stress Unit'),
                                            ('Feature Attributes', 'Outer Diameter'),
                                            ('Feature Attributes', 'Outer Diameter Unit'),
                                            ('Feature Attributes', 'Inside Diameter'),
                                            ('Feature Attributes', 'Inside Diameter Unit'),
                                            ('Feature Attributes', 'Joint Factor'),
                                            ('Feature Attributes', 'Corrosion Allowance, mm'),
                                            ('Feature Attributes', 'Insulation Status'),
                                            ('Feature Attributes', 'Schedule'),
                                            ('Feature Attributes', 'NWT, mm'),
                                            ('Feature Attributes', 'Piping Formula'),
                                            ('Feature Attributes', 'Mechanical Allowance'),
                                            ('Feature Attributes', 'Design Factor'),
                                            ('Feature Attributes', 'Temperature Factor'),
                                            ('Feature Attributes', 'CONC(ALARM1), mm'),
                                            ('Feature Attributes', 'MAWT(ALARM2), mm'),
                                            ('Feature Attributes', 'Access')])
    
    # Drop index column with numbering fron 0,1,2.......
    df = df.set_index([('Feature Attributes', 'Feature Confidence')]).rename_axis(index=None,
                                                                                  columns=('', 'Feature Confidence'))
    
    first_row = 0
    last_row = len(df) + 2
    first_col = 0
    last_col = 0
    # writer = pd.ExcelWriter('DPE_project_color.xlsx', engine='xlsxwriter')
    #writer = pd.ExcelWriter(os.path.join(subdir, rootdir) + '_circuit_loadsheet.xlsx', engine='xlsxwriter')
    writer = pd.ExcelWriter(os.path.join(subdir, user_input, sys.argv[1].split('__')[0]) + '_concatenated_loadsheet.xlsx', engine='xlsxwriter')

    df.to_excel(writer, sheet_name='Sheet1')
    workbook = writer.book
    red = workbook.add_format({'bg_color': '#FF6347'})
    amber = workbook.add_format({'bg_color': '#FFBF00'})
    green = workbook.add_format({'bg_color': '#32CD32'})
    worksheet = writer.sheets['Sheet1']
    
    worksheet.conditional_format(first_row, first_col, last_row, last_col,
                                 {'criteria': 'containing',
                                  'type': 'text',
                                  'value': 'Low',
                                  'format': red})
    worksheet.conditional_format(first_row, first_col, last_row, last_col,
                                 {'criteria': 'containing',
                                  'type': 'text',
                                  'value': 'Med',
                                  'format': amber})
    worksheet.conditional_format(first_row, first_col, last_row, last_col,
                                 {'criteria': 'containing',
                                  'type': 'text',
                                  'value': 'High',
                                  'format': green})
    writer.save()

    import zipfile

    def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ext = os.path.splitext(file)[-1].lower()
                if ext in '.xlsx' or ext in '.txt':
                    ziph.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(path, '..')))

    zipf = zipfile.ZipFile(user_input+'.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir(user_input, zipf)
    zipf.close()
   # print('successfully')

    #print(" Program start time: ", start, "\n Program end time: ", end, "\n Time taken to process the circuit:",end - start)
    status="successfully"
    code="200"

  except:
    status="Your file(s) were uploaded successfully but getting failure due to error(s)"
    code="400"

    #print(status, code)
  #url = 'https://test.aie-veracity.com/script/'
  #url = 'https://test.aie-veracity.com/ScriptML.php?'
  #payload = {"transaction_id":transaction_id, "code":code, "status":status}
  #headers = {'cache-control': "no-cache",}
  #headers = {'content-type': 'application/json'}
  #r = requests.post(url)
  #r = requests.post(url, json=payload, headers=headers, verify=False)

  url = 'https://test.aie-veracity.com/script/transaction/'+transaction_id+'/code/'+code+'/status/'+status+'/common_gpu_script'
  print(url)
  #payload = {"transaction_id":transaction_id, "code":code, "status":status}
  #headers = {'cache-control': "no-cache",}
  #headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    
    #r = requests.post(url, verify=False)
    #from urllib import request
    #req = request.Request(url, method="POST")
    #r = request.urlopen(req)
    #content = r.read()
    #print(content)

  import pycurl
  from io import BytesIO
  b_obj = BytesIO()
  crl = pycurl.Curl()
  crl.setopt(crl.URL, url)#'https://wiki.python.org/moin/BeginnersGuide')
  crl.setopt(crl.WRITEDATA, b_obj)
  crl.perform()
  crl.close()
  get_body = b_obj.getvalue()
  print('Output of GET request:\n%s' % get_body.decode('utf8'))
