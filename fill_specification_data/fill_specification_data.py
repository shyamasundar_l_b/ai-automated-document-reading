import warnings

warnings.filterwarnings("ignore")

import pandas as pd
import numpy as np
import sys

# Permanently changes the pandas settings
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)


def drop_y(df):
    # list comprehension of the cols that end with '_y'
    to_drop = [x for x in df if x.endswith('_y')]
    df.drop(to_drop, axis=1, inplace=True)

#from tkinter import filedialog
#from tkinter import *
baseSystemPath = "/var/www/html/veracity/fill_specification_data/"

#loadsheet_folder = baseSystemPath+'circuit_loadsheets_DPE'#baseSystemPath+sys.argv[1]#filedialog.askdirectory()
loadsheet_folder = baseSystemPath+sys.argv[1]#baseSystemPath+sys.argv[1]#filedialog.askdirectory()
#legend_file = baseSystemPath+'legend_sheet_DPE.xlsx'# baseSystemPath+sys.argv[2]#askopenfilename()  # show an "Open" dialog box and return the path to the selected file
legend_file = baseSystemPath+sys.argv[2]# baseSystemPath+sys.argv[2]#askopenfilename()  # show an "Open" dialog box and return the path to the selected file

#print("Select directory path where circuit level loadsheets are saved")
#root = Tk()
#root.withdraw()
#root.geometry("500x100+600+200")
#from tkinter import Tk  # from tkinter import Tk for Python 3.x
#from tkinter.filedialog import askopenfilename
#print("Select Legend file name")
# legend_file = input('')
# legend_file = "legend_sheet.xlsx"
#Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing

legend_df = pd.read_excel(legend_file)  # , engine='openpyxl')

import os

"""
yes = {'yes', 'y', 'ye', ''}
no = {'no', 'n'}

circuit_level_yn_choice = True
piping_level_yn_choice = False
print("\n Are the piping within the selected directory", os.path.splitext(loadsheet_folder),
      "operating under 205 degree celcius? \n Please respond with Y/N or yes/no.\n")
try:
    circuit_level_yn_choice = input().lower()
except:
    sys.stdout.write("Oops... You must respond with 'yes' or 'no' / 'Y' or 'N'")
"""




legend_df = legend_df.fillna('')
# print(legend_df)
legend_df.iloc[[0]] = legend_df.iloc[[0]].replace('',np.nan)
#print(legend_df.iloc[[0]])

f = open(loadsheet_folder+'/output_log.txt', 'w')

status="successfully"
code="500"
transaction_id = ''
transaction_id = sys.argv[1]
transaction_id=transaction_id.split('__')[1]
#transaction_id="isometrics__VTID8667604542".split('__')[1]
#transaction_id = "VTID3456758133"
print(transaction_id)

try:
    # print(legend_df.columns)
    for subdir, dirs, files in os.walk(loadsheet_folder):
        for file in files:
            #if file.endswith('.xlsx'):
            if file.endswith('.xls') or file.endswith('.XLS') or file.endswith('.xlsx') or file.endswith('.xlsm'):
                if file.endswith('filled.xlsx') == False:  # to avoid reading the filled file again
                    """
                    if circuit_level_yn_choice in no:
                        piping_level_yn_choice = True
                        print(
                            "\nDoes the piping", os.path.splitext(loadsheet_folder)[
                                                     0] + ".pdf operate under 205 degree celcius? \n Please respond with Y/N or yes/no")
                        try:
                            piping_level_yn_choice = input().lower()
                        except:
                            sys.stdout.write("Oops... Please respond with 'yes' or 'no' / 'Y' or 'N'. \n Please try again")
                    """


                    loadsheet_df = pd.read_excel(
                        os.path.join(subdir, file))  # , engine = 'xlsxwriter')  # , engine='openpyxl')
                    # loadsheet_df = loadsheet_df.droplevel(0, axis=1)
                    # print(loadsheet_df)
                    uniq_piping_tags_in_loadsheet = list(set(loadsheet_df['Piping Tag/ Line Number*']))

                    uniq_piping_tags_in_legend = list(set(legend_df['Piping Tag/ Line Number*']))

                    # print("uniq_piping_tags_in_loadsheet", uniq_piping_tags_in_loadsheet)
                    # print("uniq_piping_tags_in_legend_sheet", uniq_piping_tags_in_legend)

                    loadsheet_with_substr_match_df = loadsheet_df.copy()
                    loadsheet_with_substr_match_df['Insulation Status'] = "N"
                    # print(loadsheet_with_substr_match_df['Insulation Status'])
                    for substring in uniq_piping_tags_in_loadsheet:
                        # print(substring)
                        # for substring in legend_df['Piping Tag'].tolist():
                        if next((s for s in uniq_piping_tags_in_legend if str(s) in str(substring)),
                                None) != None:  # follow https://stackoverflow.com/questions/13779526/finding-a-substring-within-a-list-in-python/13779566
                            matched_string = next((s for s in uniq_piping_tags_in_legend if str(s) in str(substring)), None)
                            # print("Substring is:", substring, "\nMatched string is:", matched_string)
                            try:
                                insulation_status = substring.split(matched_string, 1)[1]
                                if insulation_status != '':
                                    # print("here")
                                    loadsheet_with_substr_match_df[
                                        'Insulation Status'] = loadsheet_with_substr_match_df.apply(
                                        lambda x: x['Insulation Status'].replace('N', insulation_status), axis=1)
                            except:
                                pass
                            # print("Insulation Status", insulation_status)
                            loadsheet_with_substr_match_df.replace(to_replace=substring, value=matched_string, regex=True,
                                                                   inplace=True)
                    # print(loadsheet_with_substr_match_df['Insulation Status'])
                    # loadsheet_with_substr_match_df = loadsheet_df[np.logical_or.reduce([loadsheet_df['Piping Tag/ Line Number*'].str.contains(str(code), na = False) for code in legend_df['Piping Tag/ Line Number*'].tolist()])]
                    for col in loadsheet_with_substr_match_df.columns:
                        try:
                            loadsheet_with_substr_match_df[col] = loadsheet_with_substr_match_df[col].astype(float)
                        except:
                            pass

                    for col in legend_df.columns:
                        try:
                            legend_df[col] = legend_df[col].astype(float)
                        except:
                            pass

                    """
                    test_df = pd.DataFrame({'Feature Type':['Elbow', 'Pipe'], 'Feature Size':[2, 9], 'Piping Tag':['02-DC-002-A1', '001-A2']})
                    test_df['Feature Size'] = test_df['Feature Size'].astype(float)
                    print(test_df)



                    print(loadsheet_with_substr_match_df[['Feature Size','Feature Type', 'Piping Tag']])
                    out_test_df = pd.merge(legend_df, test_df, on=['Feature Size','Feature Type', 'Piping Tag'])["Material Specification"].unique().tolist()
                    out_test_df
                    """
                    # print(legend_df)
                    # print("Loadsheet with substring match from legend piping tag")
                    # loadsheet_with_substr_match_df
                    # print(legend_df.columns)
                    key_columns = []
                    key_columns_y_suffix = []

                    data_columns = []
                    for col in legend_df.columns:
                        if pd.isna(legend_df.iloc[0][col]) == False:  # check for data columns targeting the key columns
                            # print("\nLegend key Column header:", col)
                            key_columns.append(col)
                            key_columns_y_suffix.append(col + '_y')
                            # print("Legend data columns are:", legend_df.iloc[0][col])
                            data_cols = legend_df.iloc[0][col].split(";")
                            data_columns.append(data_cols)

                    # print(key_columns)
                    # print(key_columns_y_suffix)
                    # print(data_columns)

                    key_columns_str = str(key_columns)[1:-1]
                    # print(key_columns_str)
                    data_columns_str = str(data_columns)[1:-1]
                    # print(data_columns_str)
                    # loadsheet_with_substr_match_df = loadsheet_with_substr_match_df.fillna("")
                    # legend_df = loadsheet_with_substr_match_df.fillna("")

                    # print("After substring match, resulting row looks like:")

                    import re

                    for data, key, key_y in zip(data_columns, key_columns, key_columns_y_suffix):
    #                    try:
                            # print(data, key, key_y)
                            # if key == "Design Code":
                            #    print(len(df),data[0])
                            #    df = (pd.concat([loadsheet_with_substr_match_df, legend_df]).groupby(data[0])[key].unique().apply(
                            #        list).reset_index())
                            # else:
                            # print(legend_df[legend_df['Feature Size, inches'].isnull()].index.tolist())



                            df = (pd.concat([loadsheet_with_substr_match_df, legend_df]).groupby(data)[key].unique().apply(
                                list).reset_index())
                            # df[data] = df[data].replace(r'\s+',np.nan,regex=True).replace('',np.nan)
                            # df = df.replace('', np.nan)

                            # print(df[data])
                            # df[data] = df[data].dropna(how='all')

                            # print(df[data])
                            # df = (loadsheet_with_substr_match_df.append(legend_df, ignore_index=True).groupby(data)[key].unique().apply(
                            #        list).reset_index())

                            # df = df.reset_index()
                            df[key] = [x for x in df[key] if str(x) != 'nan']
                            # print(df)
                            df[key] = df[key].apply(lambda x: re.sub(r"[\[\]]", "", str(x)))
                            # df['Material Grade'] = df['Material Grade'].apply(lambda x: x.replace("[", ""))
                            # df['Material Grade'] = df['Material Grade'].apply(lambda x: x.replace("]", ""))

                            # print(df[key])

                            df[key] = df[key].str.split(", ")
                            df[key] = df[key].apply(lambda x: [i for i in x if str(i) != "nan"])
                            df[key] = df[key].apply(lambda x: ' '.join([str(i) for i in x]))
                            df = df[df[key] != '']  # remove empty rows wrt the column
                            df[key] = df[key].apply(lambda x: re.sub(r"\'", "", str(x)))

                            df[data] = df[data].replace(r'\s+', np.nan, regex=True).replace('', np.nan)

                            df[key] = df[key].replace('', np.nan)
                            df = df.dropna(subset=data, how='all')

                            for col in df.columns:
                                try:
                                    df[col] = df[col].astype(float)
                                except:
                                    # print("column",col)
                                    # print(df[col])
                                    pass

                            # print(df)
                            # df['Material Grade'] = [re.sub(r"[\[\]]", "", str(x)) for x in df['Material Grade']]
                            # df = df.dropna()
                            # print(df)

                            # print(df.dtypes)
                            # print(df.columns,"\n\n\n")

                            # print(loadsheet_with_substr_match_df.dtypes)
                            # if key == "Design Code":
                            # print(len(df),data[0])
                            # loadsheet_with_substr_match_df = pd.merge(loadsheet_with_substr_match_df, df, on=data[0],
                            #                                          how='left',
                            #                                          suffixes=('', '_y'))
                            # else:

                            # for col in df.columns:

                            df_new = df.fillna(loadsheet_with_substr_match_df)
                            df_new = pd.merge(df_new,df, how = 'left')
                            #df_new = (pd.concat([df_new, df], ignore_index=True))  # .drop_duplicates()
                            df_new[key] = df_new[key].replace('', np.nan)
                            df_new = df_new.dropna()

                            # print(df)

                            #print(df_new, "\n\n\n\n")

                            loadsheet_with_substr_match_df = pd.merge(loadsheet_with_substr_match_df,
                                                                      df_new, on=data, how='left',
                                                                      suffixes=('', '_y'))

                            try:
                                loadsheet_with_substr_match_df[key] = loadsheet_with_substr_match_df[key_y].values
                            except:
                                pass

                            # =loadsheet_with_substr_match_df
    #                    except:
    #                        pass

                    drop_y(loadsheet_with_substr_match_df)
                    #try:
                    loadsheet_with_substr_match_df['NWT, mm'] = pd.to_numeric(loadsheet_with_substr_match_df['NWT, mm'], errors='coerce')
                    loadsheet_with_substr_match_df['CONC(ALARM1), mm'] = 0.875 * loadsheet_with_substr_match_df[
                        'NWT, mm'] - \
                                                                         loadsheet_with_substr_match_df[
                                                                             'Corrosion Allowance, mm']
                    loadsheet_with_substr_match_df['Inside Diameter'] = loadsheet_with_substr_match_df[
                                                                            'Outer Diameter'] - \
                                                                        2 * loadsheet_with_substr_match_df[
                                                                            'NWT, mm']
                    loadsheet_with_substr_match_df['Inside Diameter Unit'] = loadsheet_with_substr_match_df[
                        'Outer Diameter Unit']
                    loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] = (loadsheet_with_substr_match_df[
                                                                              'Design Pressure'] *
                                                                          loadsheet_with_substr_match_df[
                                                                              'Outer Diameter']) / (2 * (
                                loadsheet_with_substr_match_df['Allowable Stress'] + (
                                    0.4 * loadsheet_with_substr_match_df['Design Pressure'])))
                    #if piping_level_yn_choice in yes or circuit_level_yn_choice in yes:
                    SMT_df_LT_205 = pd.DataFrame(
                        {'Feature Size, inches': [0.75, 1, 1.5, 2, 3, 4, 6, 8, 10, 12, 14, 16, 18, 20, 24],
                         'SMT': [1.8, 1.8, 1.8, 1.8, 2.0, 2.3, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 2.8, 3.1, 3.1]})
                    SMT_df_GTE_205 = pd.DataFrame(
                        {'Feature Size, inches': [0.75, 1, 1.5, 2, 3, 4, 6, 8, 10, 12, 14, 16, 18, 20, 24],
                         'SMT': [2.0, 2.0, 2.3, 2.5, 2.8, 3.1, 3.3, 3.3, 3.3, 3.3, 3.3, 3.3, 3.3, 3.6, 3.6]})
                    #print(SMT_df)
                    """
                    loadsheet_with_substr_match_df_dummy = loadsheet_with_substr_match_df.copy()
                    match_dict = SMT_df.set_index('Feature Size, inches')['SMT'].to_dict()
                    match_column = loadsheet_with_substr_match_df_dummy.filter(like='Feature Size')
                    #print(match_column)
                    #loadsheet_with_substr_match_df_dummy[match_column.columns] = match_column.replace(match_dict)
                    loadsheet_with_substr_match_df_dummy['MAWT(ALARM2), mm'] = match_column.replace(match_dict)
                    print(loadsheet_with_substr_match_df_dummy['Feature Size, inches'], loadsheet_with_substr_match_df_dummy['MAWT(ALARM2), mm'])
                    loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] = pd.concat([loadsheet_with_substr_match_df['MAWT(ALARM2), mm'], loadsheet_with_substr_match_df_dummy['MAWT(ALARM2), mm']]).max(level=0)
                    """

                    """
                    LT_205_df = loadsheet_with_substr_match_df[loadsheet_with_substr_match_df['Design Temperature'] < 205]
                    GTE_205_df = loadsheet_with_substr_match_df[loadsheet_with_substr_match_df['Design Temperature'] >= 205]
                    SMT_df_LT_205 = pd.merge(SMT_df_LT_205, LT_205_df, on='Feature Size, inches',
                                             how='right')
                    SMT_df_GTE_205 = pd.merge(SMT_df_GTE_205, GTE_205_df, on='Feature Size, inches',
                                             how='right')
                    SMT_df = pd.merge(SMT_df_GTE_205, SMT_df_LT_205, on = 'SMT')
                    print(SMT_df)
                    """



                #if (loadsheet_with_substr_match_df[loadsheet_with_substr_match_df['Design Temperature'] < 205]).all(axis=1):
                    df_LT_205 = loadsheet_with_substr_match_df[(loadsheet_with_substr_match_df['Design Temperature'] < 205)]# (loadsheet_with_substr_match_df['Design Temperature'].lt(205)).any():# (loadsheet_with_substr_match_df.query(['Design Temperature'] < 205)):
                    SMT_df_LT_205 = pd.merge(df_LT_205, SMT_df_LT_205, on='Feature Size, inches', how='left')
                    #print(SMT_df_LT_205)
                    SMT_df_LT_205['MAWT(ALARM2), mm_new'] = SMT_df_LT_205[["MAWT(ALARM2), mm", "SMT"]].max(axis=1)
                    #loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] = SMT_df_LT_205['MAWT(ALARM2), mm_new']
                #elif (loadsheet_with_substr_match_df[loadsheet_with_substr_match_df['Design Temperature'] >= 205]).all(axis=1):
                    df_GTE_205 = loadsheet_with_substr_match_df[(loadsheet_with_substr_match_df['Design Temperature'] >= 205)] #(loadsheet_with_substr_match_df['Design Temperature'].gt(205)).any():
                    SMT_df_GTE_205 = pd.merge(df_GTE_205, SMT_df_GTE_205, on='Feature Size, inches', how='left')

                    #print(SMT_df_GTE_205)
                    SMT_df_GTE_205['MAWT(ALARM2), mm_new'] = SMT_df_GTE_205[["MAWT(ALARM2), mm", "SMT"]].max(axis=1)
                    #loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] = SMT_df_GTE_205['MAWT(ALARM2), mm_new']
                    SMT_df = pd.concat([SMT_df_LT_205, SMT_df_GTE_205], ignore_index=True)
                    #print(SMT_df)
                    loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] = SMT_df['MAWT(ALARM2), mm_new']
                    mawt_condition = (loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] < 2.5)
                    loadsheet_with_substr_match_df.loc[mawt_condition, 'MAWT(ALARM2), mm'] = 2.5
                    replaced_rows = loadsheet_with_substr_match_df.loc[
                        loadsheet_with_substr_match_df['MAWT(ALARM2), mm'] < 2.5].reset_index().index
                    print("\nNumber of MAWTs with value <2.5 in ",
                          os.path.splitext(file)[0] + ".xlsx got replaced are: ", len(replaced_rows))
                    f.write("\nNumber of MAWTs with value <2.5 in " +
                          os.path.splitext(file)[0] + ".xlsx got replaced are: " + str(len(replaced_rows)) + "\n" )
                    loadsheet_with_substr_match_df['Piping Tag/ Line Number*'] = loadsheet_df[
                        'Piping Tag/ Line Number*']
    #                except:
    #                    pass
                    """
                    loadsheet_with_substr_match_df.columns = pd.MultiIndex.from_product([['Feature Attributes'], loadsheet_with_substr_match_df.columns])
                    writer = pd.ExcelWriter(os.path.join(subdir,os.path.splitext(file)[0])+'_filled.xlsx', engine='xlsxwriter')
                    sheetname = os.path.join(os.path.splitext(file)[0] + '.xlsx')
                    loadsheet_with_substr_match_df.to_excel(writer, sheet_name=sheetname)
                    worksheet = writer.sheets[sheetname]  # pull worksheet object
                    for idx, col in enumerate(loadsheet_with_substr_match_df):  # loop through all columns
                        series = loadsheet[col]
                        max_len = max((
                            series.astype(str).map(len).max(),  # len of largest item
                            len(str(series.name))  # len of column name/header
                        )) + 1  # adding a little extra space
                        worksheet.set_column(idx, idx, max_len)  # set column width
                    writer.save()
                    """
                    # print(loadsheet_with_substr_match_df)
                    loadsheet_with_substr_match_df.to_excel(
                        os.path.join(subdir, os.path.splitext(file)[0] + '_filled.xlsx'), index=False, header=True)
    f.close()

    import zipfile

    def zipdir(path, ziph):
        # ziph is zipfile handle
        for root, dirs, files in os.walk(path):
            for file in files:
                ext = os.path.splitext(file)[-1].lower()
                if file.endswith('filled.xlsx') or file.endswith('.txt'):
                    ziph.write(os.path.join(root, file),
                           os.path.relpath(os.path.join(root, file),
                                           os.path.join(path, '..')))

    zipf = zipfile.ZipFile(loadsheet_folder+'.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir(loadsheet_folder, zipf)
    zipf.close()

    #import shutil

    #shutil.rmtree(loadsheet_folder, ignore_errors=True)
    #os.remove(legend_file)

    """
    import time

    print("\nFilled-in Loadsheets are saved in the directory", loadsheet_folder,"\n\n Output log is saved in the file output_log.txt where you executed the program" "\n\n Window closes in 7 seconds: ")
    def countdown(t):
        while t:
            mins, secs = divmod(t, 60)
            time.sleep(1)
            timeformat = '{:02d}:{:02d}'.format(mins, secs)
            print(timeformat + 's  ---->', end="\t", flush=True)  # , end='\r')
            # sys.stdout.flush()
            t -= 1
        # print('Goodbye!\n\n\n\n\n')


    countdown(7)
    """
    print('successfully')

    status="successfully"
    code="200"
except:
    status="Your file(s) were uploaded successfully but getting failure due to error(s)"
    code="400"

url = 'https://test.aie-veracity.com/script/transaction/'+transaction_id+'/code/'+code+'/status/'+status+'/common_gpu_script'
print(url)
import pycurl
from io import BytesIO
b_obj = BytesIO()
crl = pycurl.Curl()
crl.setopt(crl.URL, url)#'https://wiki.python.org/moin/BeginnersGuide')
crl.setopt(crl.WRITEDATA, b_obj)
crl.perform()
crl.close()
get_body = b_obj.getvalue()
print('Output of GET request:\n%s' % get_body.decode('utf8'))
