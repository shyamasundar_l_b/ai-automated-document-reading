import pandas as pd
import numpy as np
import time
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
# Permanently changes the pandas settings
pd.set_option('display.max_rows', None)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', None)


import re
from os import listdir

from tkinter.filedialog import askopenfilename
import os
import re

from os import listdir
from tkinter import filedialog
from tkinter import *

project_number = input("Enter the number to select the corresponding project: \n 1. DPE project \n 2. DNO project \n 3. JV project\n")

print("Select the circuit level directory where Inspection Reports are stored")
root = Tk()
root.withdraw()
root.geometry("500x100+600+200")
user_input = filedialog.askdirectory()

#user_input = input("Enter the directory path where dxf files are saved: \n")

assert os.path.exists(user_input), "Path not found, "+str(user_input)
#print(user_input)
rootdir = user_input
def nextword(target, source):
    for i, w in enumerate(source):
        if w == target:
            return source[i + 1]


def replace_x_with_y_and_drop_y_and_rename_headers(df):
    # list comprehension of the cols that end with '_y'
    to_copy = [x for x in df if x.endswith('_y')]
    # print(to_copy)
    to_replace = [x for x in df if x.endswith('_x')]
    # print(to_replace)

    for copy, replace in zip(to_copy, to_replace):
        # print(copy,replace)
        df[to_replace] = df[to_copy].values
    df.drop(to_copy, axis=1, inplace=True)
    df.columns = df.columns.str.replace("_x", "")
    df.columns = df.columns.str.replace("\.+\d+", "")


import dateutil.parser as dparser


def parse_datetime_remove_useless_end(date_str):
    for i in range(len(date_str) + 1, 0, -1):
        try:
            # print(date_str[:i],"\n\n\n\n")
            return dparser.parse(date_str[i:]).strftime("%Y-%m-%d")
        except ValueError:
            # print(date_str[:i],"\n\n\n\n")
            pass


import os
from glob import glob
from pathlib import Path

df_concatenated_in_loop = pd.DataFrame()

num_sub_folders = len(next(os.walk(rootdir))[1])

from configparser import ConfigParser
config = ConfigParser()

config.read('config.ini')

if num_sub_folders==0:
    for subdir, dirs, files in os.walk(rootdir):

        #print(subdir, dirs, files)
        dirs=os.path.basename(subdir).split("!@#$%^&*") #convert string to list if there is only one circuit present in the directory
        #print(dirs)

        for directory in dirs:
            #print(dirs)
            #print(os.path.basename(subdir))
            if project_number == '1':
                print("Select the pre-populated loadsheet to fill with the extracted thickness readings for the circuit", directory)
                # legend_file = input('')
                # legend_file = "legend_sheet.xlsx"

                Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
                DPE_loadsheet = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
                start_time = time.time()

                DPE_loadsheet_filled_df = pd.read_excel(DPE_loadsheet, header=2)  # , engine='openpyxl')  # , engine='openpyxl')
                DPE_loadsheet_filled_df_multiIndex = pd.read_excel(DPE_loadsheet, header=[0, 1, 2])  # , engine='openpyxl')
                DPE_loadsheet_filled_df_orig = DPE_loadsheet_filled_df.copy()

                #for directory in dirs:
                #print(directory)
                df_concatenated_in_loop = pd.DataFrame()
                print("\nProcessing circuit ", directory,"\n")
                circuit_name = os.fsencode(os.path.join(subdir))
                for file in os.listdir(circuit_name):
                    file = os.fsdecode(file)
                    #print(file)
                    if file.endswith('.xls') or file.endswith('.xlsx') or file.endswith('.xlsm'):
                        if file.endswith('for_QC.xlsx') == False:# or file.endswith('validated.xlsx') == False:
                            try:
                                print("Processing report ", file)
                                test_df = pd.read_excel(os.path.join(subdir, file))

                                test_df = test_df.replace('\n', ' ', regex=True)
                                test_df = test_df.replace(np.nan, ' ', regex=True)
                                # display(test_df)
                                try:
                                    line_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'line_number_str1')).any(level=0)]
                                    # print(line_number)
                                    line_number = str(line_number.apply(''.join, axis=1))
                                    line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                    #line_number = re.search('(?<=LINE NO. )(\w[\w-]+)', line_number).group()
                                    line_number = re.search('(?<='+re.escape(config.get('string_match', 'line_number_str1'))+' )[^.\s]*', line_number).group()
                                    #line_number = re.search('(?<=LINE NO. )[^.\s]*', line_number).group()
                                    #print("Line number: ", line_number)
                                except:
                                    line_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'line_number_str2')).any(level=0)]
                                    # print(line_number)
                                    line_number = str(line_number.apply(''.join, axis=1))
                                    line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                    #line_number = re.search('(?<=LINE NO )(\w[\w-]+)', line_number).group()
                                    line_number = re.search('(?<='+re.escape(config.get('string_match', 'line_number_str2'))+' )[^.\s]*', line_number).group()
                                    #line_number = re.search('(?<=LINE NO )[^.\s]*', line_number).group()
                                    #print("Line number: ", line_number)

                                try:
                                    rep_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'report_number_str1')).any(level=0)]
                                    rep_number = str(rep_number.apply(''.join, axis=1))
                                    rep_number = ' '.join(rep_number.split())  # remove extra whitespaces >1
                                    rep_number = re.search('(?<='+re.escape(config.get('string_match', 'report_number_str1'))+' )[^.\s]*', rep_number).group()
                                    #print("Report number: ", rep_number)
                                except:
                                    rep_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'report_number_str2')).any(level=0)]
                                    rep_number = str(rep_number.apply(''.join, axis=1))
                                    rep_number = ' '.join(rep_number.split())  # remove extra whitespaces >1
                                    rep_number = re.search('(?<='+re.escape(config.get('string_match', 'report_number_str2'))+' )[^.\s]*', rep_number).group()
                                    #print("Report number: ", rep_number)

                                try:
                                    taken_by_copy = ''
                                    taken_by_copy = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'taken_by_start_str1')).any(level=0)]
                                    taken_by_copy = str(taken_by_copy.apply(''.join, axis=1))
                                    taken_by_copy = ' '.join(taken_by_copy.split())  # remove extra whitespaces
                                    start_pts = [i + len(config.get('string_match', 'taken_by_start_str1')) for i in range(len(taken_by_copy)) if
                                                 taken_by_copy.startswith(config.get('string_match', 'taken_by_start_str1'), i)]
                                    end_pts = [i for i in range(len(taken_by_copy)) if taken_by_copy.startswith(config.get('string_match', 'taken_by_end_str1'), i)]
                                    taken_by = ''
                                    for i in range(len(start_pts)):
                                        taken_by = taken_by + taken_by_copy[start_pts[i]:end_pts[i]] + ','
                                    # taken_by = re.search('(?<=INSPECTION TECHNICIAN )(\w[\w-]+)',taken_by).group()
                                    taken_by = taken_by[:-2]  # remove last comma appended
                                    #print("Taken by: ", taken_by)
                                except:
                                    taken_by = None

                                try:
                                    date = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'date_str1')).any(level=0)].to_string(
                                        header=False, index=False)
                                    # date = date.apply(''.join, axis=1)
                                    date = ' '.join(date.split())  # remove extra whitespaces >1
                                    # print(date)

                                    # date = re.search('(?<=DATE OF INSPECTION )(\w[\w-]+)',date).group()
                                    # print("Date js: ", date)

                                    """
                                    date = date.replace('\n','')
                                    date = date.replace('\\','')
                                    date = date.replace('Unnamed:','')
                                    date = date.replace(':','')
                                    #date = date.replace('-','/')
                                    date = ' '.join(date.split())
                                    #date = dparser.parse(str(date),fuzzy=True).strftime("%Y-%m-%d")
                                    """
                                    date = date.partition(config.get('string_match', 'date_str1'))[2]
                                    date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                    #print(date)

                                    # date = datetime.strptime(date.group(), '%Y-%m-%d').date()
                                    # date = parse_datetime_remove_useless_end(date)
                                    # print("Date: ", date)
                                except:
                                    from _datetime import datetime
                                    date = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'date_str2')).any(level=0)].to_string(
                                        header=False, index=False)
                                    date = ' '.join(date.split())  # remove extra whitespaces >1
                                    date = date.partition(config.get('string_match', 'date_str2'))[2]
                                    #date = re.search('(?<=INSPECTION DATE)(\w[\w-]+)',date).group()
                                    #print("Date is: ", date)
                                    date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")

                                    #date = datetime.strptime(date.group(), '%Y-%m-%d').date()
                                    #date = parse_datetime_remove_useless_end(date)
                                    #print("Date: ", date)
                                    #print(date)

                        # header_row_index = test_df.index[test_df.stack().str.contains('t-ALERT').any(level=0)].to_list()[0]#.index
                                header_row_index = \
                                test_df.index[test_df.stack().str.contains(config.get('header_match', 'header_row_match_str1')).any(level=0)].to_list()[0]  # .index
                                test_df.columns = test_df.iloc[header_row_index]

                                processed_df = test_df.iloc[header_row_index + 1:].reset_index(drop=True)

                                processed_df.rename(columns={header_row_index: None, config.get('header_match', 'feature_type_str1'): 'Feature Type', config.get('header_match', 'feature_ID_str1'): 'Feature ID', config.get('header_match', 'feature_size_str1'): 'Feature Size', config.get('header_match', 'minimum_WT_str1'): 'Minimum WT', config.get('header_match', 'maximum_WT_str1'): 'MaximumWT', config.get('header_match', 'nwt_str1'): 'NWT, mm', config.get('header_match', 'header_row_match_str1'): 'Comment/\nRemarks'},inplace=True)
                                processed_df['Date 1*\n(YYYY-MM-DD)'] = date
                                processed_df['Piping Tag/ Line Number*'] = line_number
                                processed_df['Report Number'] = rep_number
                                processed_df['Taken by'] = taken_by

                                processed_df['Average WT'] = (pd.to_numeric(processed_df['Minimum WT'],
                                                                            errors='coerce') + pd.to_numeric(processed_df['MaximumWT'],
                                                                                                             errors='coerce')) / 2

                                processed_df['Comment/\nRemarks'] = 'Inspection Findings: ' + processed_df['Comment/\nRemarks'].map(
                                    str) + '\nRecommended Action: ' + processed_df['RECOMMENDED ACTION'].map(str)
                                processed_df.drop([config.get('header_match', 'loss_str1'), config.get('header_match', 'recommended_action_str1'), config.get('header_match', 'photo_str1')], axis=1, inplace=True)

                                # processed_df.columns = ['Feature ID',	'Feature Type', 'Feature Size', 't-ANOMALY',	'Minimum WT',	'Maximum WT',	'Average WT', 't-ALERT',	'LOSS',	'Comment/Remarks', 'RECOMMENDED ACTION', 'PHOTO']
                                processed_df = processed_df[
                                    ['Piping Tag/ Line Number*', 'Feature Type', 'Feature ID', 'Feature Size', 'NWT, mm',
                                     'Date 1*\n(YYYY-MM-DD)', 'Minimum WT', 'MaximumWT', 'Average WT', 'Report Number', 'Taken by',
                                     'Comment/\nRemarks']]

                                df_concatenated_in_loop = df_concatenated_in_loop.append(processed_df)
                            except:
                                pass

                                # display(processed_df.columns)
                # display(processed_df['Feature Size'].to_string(index=False))

                #print(DPE_loadsheet_filled_df)
                #print(df_concatenated_in_loop)
                #df_concatenated_in_loop = df_concatenated_in_loop.replace(' ',np.NaN, regex=True)
                df_concatenated_in_loop = df_concatenated_in_loop[df_concatenated_in_loop['Feature ID'] != ' ']
                #df_concatenated_in_loop['Piping Tag/ Line Number*'] = df_concatenated_in_loop['Piping Tag/ Line Number*'].replace('#','x',regex=True)
                #df_concatenated_in_loop.to_excel("df_concatenated_in_loop.xlsx", index=False)


                matched_entries = pd.merge(DPE_loadsheet_filled_df, df_concatenated_in_loop,
                                           on=['Piping Tag/ Line Number*', 'Feature ID'], how='inner',
                                           right_index=True)  # ,  indicator=True)
                unmatched_entries = pd.merge(DPE_loadsheet_filled_df, matched_entries, on=['Piping Tag/ Line Number*', 'Feature ID'],
                                             how='left', indicator=True).query('_merge != "both"').drop('_merge', 1)

                replace_x_with_y_and_drop_y_and_rename_headers(unmatched_entries)
                replace_x_with_y_and_drop_y_and_rename_headers(matched_entries)

                unmatched_entries = unmatched_entries.iloc[:, :-10]
                #print(matched_entries)
                DPE_loadsheet_filled_df = pd.concat([unmatched_entries, matched_entries], join='outer')  # .reset_index(drop=True)

                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.drop_duplicates()
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.sort_values(by="Date 1*\n(YYYY-MM-DD)", key=pd.to_datetime, ascending=False)

                #DPE_loadsheet_filled_df['Date 1*\n(YYYY-MM-DD)'] = pd.to_datetime(DPE_loadsheet_filled_df['Date 1*\n(YYYY-MM-DD)'], format='%Y-%m-%d').dt.date
                #DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.sort_values(by="Date 1*\n(YYYY-MM-DD)", ascending=False)


                # DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.groupby('Feature ID')
                #print(DPE_loadsheet_filled_df)


                # filter entries having multiple year readings
                grouped_df = DPE_loadsheet_filled_df.groupby(level=0).filter(lambda x: len(x) > 1)  # .transform('count') > 1].dropna(axis = 0, how = 'all').reset_index()#.shift(19, axis=1)#.apply(lambda x: ','.join(x.astype(str)))
                #grouped_df = DPE_loadsheet_filled_df.groupby(['Piping Tag/ Line Number*', 'Feature ID']).filter(lambda x: len(x) > 1)
                #print(grouped_df)
                # grouped_df_rows = grouped_df.groupby(['Feature ID'])

                grouped_df['Group'] = grouped_df.groupby(['Piping Tag/ Line Number*', 'Feature ID']).cumcount()
                #print(grouped_df)
                dfs = dict(tuple(grouped_df.groupby('Group')))

                incr_index = 12
                start = 7
                interval_list = []

                for i in range(start, len(grouped_df.columns), incr_index):
                    interval_list.append(i)

                counter2 = 0
                grouped_dfs_concat = pd.DataFrame()


                def generate_df_from_grouped(dfs, index):
                    # dfs_0=None
                    counter = 0
                    for i in range(len(dfs)):

                        # print("i=",i,"counter=",counter)
                        # display(dfs[i])
                        if counter < len(dfs):
                            try:
                                globals()['dfs_{0}'.format(i)] = dfs[i].iloc[index].reset_index().set_index('index').T
                            except:
                                pass
                            counter += 1

                    # global dfs_0

                    for intr, ind in zip(range(len(interval_list)), range(len(dfs))):
                        # print(intr,ind)
                        try:
                            dfs_0.iloc[:, interval_list[intr]:interval_list[intr + 1]] = globals()['dfs_{0}'.format(ind)].iloc[:,
                                                                                         start:start + incr_index].values
                        except:
                            pass
                        # print(interval_list[intr])
                    # display(globals()['dfs_{0}'.format(index)])
                    globals()['dfs_{0}'.format(i)] = None

                    return (dfs_0)

                    # dfs_0=None


                for counter2 in range(len(grouped_df['Group'])):
                    # print("Counter2=",counter2)

                    dfs_received = generate_df_from_grouped(dfs, counter2)

                    grouped_dfs_concat = grouped_dfs_concat.append(dfs_received)
                    # dfs_0=None
                try:
                    del grouped_dfs_concat['Group']
                except:
                    pass

                grouped_dfs_concat.index
                #print(grouped_dfs_concat)
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df[~DPE_loadsheet_filled_df.index.isin(grouped_dfs_concat.index)]
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.append(grouped_dfs_concat)
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.sort_index(axis=0, ascending=True)
                #print(DPE_loadsheet_filled_df.head())



                df_concatenated_in_loop_new = df_concatenated_in_loop.copy()
                print(df_concatenated_in_loop_new.head())
                df_concatenated_in_loop_new = df_concatenated_in_loop_new[
                    (df_concatenated_in_loop_new['Feature Type'] != 'MIN') & (
                                df_concatenated_in_loop_new['Feature Type'] != 'MAX') & (
                                df_concatenated_in_loop_new['Feature Type'] != ' ')]
                df_concatenated_in_loop_new['Feature ID'] = df_concatenated_in_loop_new['Feature ID'].astype(str)
                DPE_loadsheet_filled_df_orig['Feature ID'] = DPE_loadsheet_filled_df_orig['Feature ID'].astype(str)

                df_fill_unmatched_entries = DPE_loadsheet_filled_df_orig.merge(
                    df_concatenated_in_loop_new.drop_duplicates(), on=['Piping Tag/ Line Number*', 'Feature ID'],
                    how='right', indicator=True).query('_merge == "right_only"').drop('_merge', 1)
                replace_x_with_y_and_drop_y_and_rename_headers(df_fill_unmatched_entries)
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.append(df_fill_unmatched_entries)



                DPE_loadsheet_filled_df.columns = DPE_loadsheet_filled_df_multiIndex.columns
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.set_index(
                    [('Circuit Tag', 'Automatically filled from piping loadsheet', 'Piping Tag/ Line Number*')]).rename_axis(index=None,
                                                                                                                             columns=(
                                                                                                                             'Circuit Tag',
                                                                                                                             'Automatically filled from piping loadsheet',
                                                                                                                            'Piping Tag/ Line Number*'))
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.drop_duplicates()

                #writer = pd.ExcelWriter('circuit_level_DPE_IR_for_QC.xlsx', engine='xlsxwriter')
                #DPE_loadsheet_filled_df.to_excel(writer, sheet_name='Sheet1')
                #writer.sheets['Sheet1'].set_row(2, None, None, {'hidden': True})
                #writer.save()
                #print(DPE_loadsheet_filled_df.head())
                DPE_loadsheet_filled_df.to_excel(
                        os.path.join(subdir, directory + '_for_QC.xlsx'))  # , engine='openpyxl')

                from openpyxl import Workbook, load_workbook
                from openpyxl.styles import Alignment

                wb = load_workbook(os.path.join(subdir, directory + '_for_QC.xlsx'))
                sheet = wb.active
                sheet.merge_cells('A1:C1')
                cell = sheet.cell(row=1, column=1)
                cell.value = 'Circuit Tag'

                sheet.merge_cells('A2:G2')
                cell = sheet.cell(row=2, column=1)
                cell.value = 'Automatically filled from piping loadsheet'


                cell.alignment = Alignment(horizontal='center', vertical='center')

                sheet.delete_rows(4)
                wb.save(os.path.join(subdir, directory + '_for_QC.xlsx'))

                end_time = time.time()
                path, dirs, files = next(os.walk(os.path.join(subdir)))
                file_count = len(files)

                print("\n Time taken to process the circuit", directory," with ",file_count,"Inspection Reports: ", end_time - start_time,"seconds\n")

            if project_number == '2':
                print(
                    "Select the pre-populated loadsheet to fill with the extracted thickness readings for the circuit", directory)
                # legend_file = input('')
                # legend_file = "legend_sheet.xlsx"

                Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
                DNO_loadsheet = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
                start_time = time.time()

                DNO_loadsheet_filled_df = pd.read_excel(DNO_loadsheet,
                                                        header=2)  # , engine='openpyxl')  # , engine='openpyxl')
                DNO_loadsheet_filled_df_multiIndex = pd.read_excel(DNO_loadsheet,
                                                                   header=[0, 1, 2])  # , engine='openpyxl')
                DNO_loadsheet_filled_df_orig = DNO_loadsheet_filled_df.copy()
                df_concatenated_in_loop = pd.DataFrame()
                print("\nProcessing circuit ", directory, "\n")
                circuit_name = os.fsencode(os.path.join(subdir))
                pattern = r'(?i)(.*Thickness.*)'
                for file in os.listdir(circuit_name):
                    file = os.fsdecode(file)

                    if file.endswith('for_QC.xlsx') == False:
                        #print(file)
                        if file.endswith('.xls') or file.endswith('.XLS') or file.endswith('.xlsx') or file.endswith('.xlsm'):
                          # or file.endswith('validated.xlsx') == False:
                            print("Processing report:", file)
                            test_df = pd.ExcelFile(os.path.join(subdir, file))



                            # print("sheetNames",test_df.sheet_names)
                            for sheet in test_df.sheet_names:
                                if re.search(pattern, sheet):  # when matching pattern add the dataframe to the list
                                    # print(file)
                                    test_df = pd.read_excel(os.path.join(subdir, file), sheet_name=sheet)
                                    test_df = test_df.replace('\n', ' ', regex=True)
                                    test_df = test_df.replace(np.nan, ' ', regex=True)
                                    # display(test_df)

                                    try:
                                        line_number = test_df.loc[
                                            test_df.stack().str.contains(
                                                config.get('string_match_DNO', 'line_number_str1')).any(
                                                level=0)]
                                        # print(line_number)
                                        line_number = str(line_number.apply(''.join, axis=1))
                                        line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                        # print("Line number: ", line_number)
                                        # print(line_number.split(config.get('string_match', 'line_number_str3'))[1])
                                        # line_number = re.search('(?<=Line No: )(\w[\w-]+)', line_number).group()
                                        line_number = re.search(
                                            '(?<=' + re.escape(
                                                config.get('string_match_DNO', 'line_number_str1')) + ' )[^.\s]*',
                                            line_number).group()
                                        # line_number = re.search('(?<=LINE NO. )[^.\s]*', line_number).group()
                                        # print("Line number: ", line_number)
                                    except:
                                        line_number = test_df.loc[
                                            test_df.stack().str.contains(
                                                config.get('string_match_DNO', 'line_number_str2')).any(
                                                level=0)]
                                        # print(line_number)
                                        line_number = str(line_number.apply(''.join, axis=1))
                                        line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                        # print("Line number: ", line_number)
                                        # print(line_number.split(config.get('string_match', 'line_number_str3'))[1])
                                        # line_number = re.search('(?<=Line No: )(\w[\w-]+)', line_number).group()
                                        line_number = re.search(
                                            '(?<=' + re.escape(
                                                config.get('string_match_DNO', 'line_number_str2')) + ' )[^.\s]*',
                                            line_number).group()
                                        # line_number = re.search('(?<=LINE NO. )[^.\s]*', line_number).group()
                                        # print("Line number: ", line_number)

                                    rep_number = test_df.loc[
                                        test_df.stack().str.contains(
                                            config.get('string_match_DNO', 'report_number_str1')).any(
                                            level=0)]
                                    rep_number = str(rep_number.apply(''.join, axis=1))
                                    rep_number = ' '.join(rep_number.split())  # remove extra whitespaces >1
                                    rep_number = re.search(
                                        '(?<=' + re.escape(
                                            config.get('string_match_DNO', 'report_number_str1')) + ' )[^.\s]*',
                                        rep_number).group()
                                    # print("rep_number: ", rep_number)

                                    try:
                                        taken_by_copy = ''
                                        taken_by_copy = test_df.loc[
                                            test_df.stack().str.contains(
                                                config.get('string_match_DNO', 'taken_by_start_str1')).any(
                                                level=0)]
                                        taken_by_copy = str(taken_by_copy.apply(''.join, axis=1))
                                        taken_by_copy = ' '.join(taken_by_copy.split())  # remove extra whitespaces
                                        start_pts = [i + len(config.get('string_match_DNO', 'taken_by_start_str1')) for
                                                     i in
                                                     range(len(taken_by_copy)) if
                                                     taken_by_copy.startswith(
                                                         config.get('string_match_DNO', 'taken_by_start_str1'),
                                                         i)]
                                        end_pts = [i for i in range(len(taken_by_copy)) if
                                                   taken_by_copy.startswith(
                                                       config.get('string_match_DNO', 'taken_by_end_str1'), i)]
                                        taken_by = ''
                                        for i in range(len(start_pts)):
                                            taken_by = taken_by + taken_by_copy[start_pts[i]:end_pts[i]] + ','
                                        # taken_by = re.search('(?<=INSPECTION TECHNICIAN )(\w[\w-]+)',taken_by).group()
                                        taken_by = taken_by[:-2]  # remove last comma appended
                                        # print("Taken by: ", taken_by)
                                    except:
                                        taken_by = None

                                    """
                                    date = test_df.loc[test_df.stack().str.contains(config.get('string_match_DNO', 'date_str1')).any(level=0)]
                                    date = str(date.apply(''.join, axis=1))
                                    date = ' '.join(date.split())  # remove extra whitespaces >1
                                    date = re.search('(?<='+re.escape(config.get('string_match_DNO', 'date_str1'))+' )[^.\s]*', date).group()
                                    date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                    print("Date:", date)
                                    """
                                    try:
                                        header_row_index = test_df.index[test_df.stack().str.contains(
                                            config.get('header_match_DNO', 'header_row_match_str1')).any(
                                            level=0)].to_list()[
                                            0]  # .index
                                        header_row_str = config.get('header_match_DNO', 'header_row_match_str1')
                                    except:
                                        header_row_index = test_df.index[test_df.stack().str.contains(
                                            config.get('header_match_DNO', 'header_row_match_str2')).any(
                                            level=0)].to_list()[
                                            0]  # .index
                                        header_row_str = config.get('header_match_DNO', 'header_row_match_str2')
                                    test_df.columns = test_df.iloc[header_row_index]

                                    for col in test_df.columns:
                                        test_df.rename(columns={col: ' '.join(col.split())},
                                                       inplace=True)  # remove extra whitespaces in column names
                                    processed_df = test_df.iloc[header_row_index + 1:].reset_index(drop=True)
                                    processed_df.rename(columns={header_row_index: None, config.get('header_match_DNO',
                                                                                                    'feature_type_str1'): 'Feature Type',
                                                                 config.get('header_match_DNO',
                                                                            'feature_ID_str1'): 'Feature ID',
                                                                 header_row_str: 'Feature Size',
                                                                 config.get('header_match_DNO',
                                                                            'remarks_str1'): 'Comment/\nRemarks'},
                                                        inplace=True)
                                    processed_df['Feature ID'] = processed_df['Feature ID'].str.strip()
                                    processed_df['Feature ID'] = processed_df['Feature ID'].str.split("-", expand=True)[
                                        0]
                                    processed_df['Feature ID'] = processed_df['Feature ID'].str.strip()
                                    processed_df["Comment/\nRemarks"] = processed_df["Comment/\nRemarks"].replace('"',
                                                                                                                  '',
                                                                                                                  regex=True)

                                    thickness_cols = processed_df.filter(like='Thickness', axis=1)
                                    # print(thickness_cols.columns)
                                    # processed_df[col] = processed_df[col].apply(lambda x: [i for i in x if i == i])

                                    for col in thickness_cols:
                                        processed_df[col] = pd.to_numeric(processed_df[col], errors='coerce')

                                    # display(processed_df)
                                    processed_df = processed_df.groupby('Feature ID', as_index=False).agg(
                                        lambda x: x.tolist())
                                    # display(processed_df)
                                    processed_df['Piping Tag/ Line Number*'] = line_number
                                    processed_df['Piping Tag/ Line Number*'] = processed_df[
                                        'Piping Tag/ Line Number*'].replace(
                                        "''", "\"", regex=True)
                                    processed_df["Feature Type"] = processed_df['Feature Type'].map(np.unique)
                                    processed_df["Feature Size"] = processed_df['Feature Size'].map(np.unique)
                                    processed_df["Comment/\nRemarks"] = processed_df['Comment/\nRemarks'].map(np.unique)

                                    processed_df["Feature Type"] = processed_df['Feature Type'].str[
                                        0]  # remove square braces
                                    processed_df["Feature Size"] = processed_df['Feature Size'].str[0]
                                    processed_df["Comment/\nRemarks"] = processed_df['Comment/\nRemarks'].apply(
                                        lambda x: ','.join(map(str, x)))
                                    processed_df["Comment/\nRemarks"] = processed_df["Comment/\nRemarks"].replace(',',
                                                                                                                  '',
                                                                                                                  regex=True)
                                    # processed_df['Comment/\nRemarks'] = processed_df['Comment/\nRemarks'].str.replace('"','')
                                    # processed_df["Comment/\nRemarks"]=processed_df['Comment/\nRemarks']

                                    thickness_cols = processed_df.filter(like='Thickness', axis=1)
                                    thickness_cols_names = thickness_cols.columns
                                    for col in thickness_cols.columns:
                                        thickness_cols[col] = thickness_cols[col].apply(
                                            lambda x: [i for i in x if i == i]).apply(
                                            lambda y: np.nan if len(
                                                y) == 0 else y)  # two apply methods. First to remove all nan's from list. This results in empty list. Second apply to replace empty list with NaN
                                        date = ''.join(col)
                                        date = ' '.join(date.split())  # remove extra whitespaces >1
                                        # date = re.search('(?<='+re.escape(config.get('string_match_DNO', 'date_str1'))+' )[^.\s]*', date).group()
                                        date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                        for val in thickness_cols[col]:

                                            if val is not np.nan:
                                                val = val.append(date)
                                    # display(thickness_cols)
                                    # date_cols = pd.concat([v.assign(date_col=k) for k, v in thickness_cols.groupby(axis=1,level=0,sort=False)],axis=1)
                                    # display(date_cols)
                                    thickness_cols = thickness_cols.apply(lambda x: pd.Series(x.dropna().values),
                                                                          axis=1)  # move nans to the end and make the readings to be continuous among dates
                                    # display(shifted_df)
                                    thickness_cols.columns = thickness_cols_names
                                    # display(thickness_cols)

                                    for ind, col in zip(range(0, len(thickness_cols.columns), 1),
                                                        thickness_cols.columns):
                                        processed_df[col] = thickness_cols[col]
                                        processed_df[col] = [[np.NaN, np.NaN, np.NaN, np.NaN] if x is np.NaN else x for
                                                             x in
                                                             processed_df[
                                                                 col]]  # replace NaN with a list of four nan's as earlier
                                        # display(thickness_cols[col])

                                        #print("index", ind)
                                        ind = str(ind)
                                        """
                                        date = ''.join(col)
                                        date = ' '.join(date.split())  # remove extra whitespaces >1
                                        #date = re.search('(?<='+re.escape(config.get('string_match_DNO', 'date_str1'))+' )[^.\s]*', date).group()
                                        date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                        #print("Date:", date)

                                        #processed_df[date+'Date 1*\n(YYYY-MM-DD)'] = date

                                        #processed_df[col] = processed_df[col].apply(lambda d: np.nan if d is np.nan else col+processed_df[col].astype(str)) #col+thickness_cols[col].astype(str)
                                        processed_df['Date_'+ind] = date
                                        """
                                        processed_df['Date_' + ind] = processed_df[col].str[-1]
                                        # display(pd.DataFrame(processed_df[col].values.tolist()))
                                        for i in processed_df[col]:
                                            i.pop(-1)
                                        processed_df = processed_df.join(
                                            pd.DataFrame(processed_df[col].values.tolist()).add_prefix(
                                                'Reading_@_' + ind))

                                        # display(processed_df)
                                        processed_df['Min_WT' + ind] = [min(x) for x in processed_df[col].tolist()]
                                        processed_df['Max_WT' + ind] = [max(x) for x in processed_df[col].tolist()]
                                        # df['Avg_WT_1'] = [sum([ float(n) for n in x ])/len(x) for x in df.Thickness_1.tolist()]
                                        # processed_df['Avg_WT_1'] = [np.array(x).astype(np.float).mean() for x in processed_df[col].values]

                                        processed_df['Avg_WT' + ind] = [sum([float(n) for n in x]) / len(x) for x in
                                                                        processed_df[col].tolist()]
                                        # display(processed_df)
                                        processed_df['Report Number' + ind] = rep_number
                                        processed_df['Taken by' + ind] = taken_by
                                        processed_df['Comment/\nRemarks' + ind] = processed_df['Comment/\nRemarks']
                                        processed_df['Probe ID' + ind] = ''
                                        processed_df['Taken by' + ind] = np.where(~processed_df['Date_' + ind].isnull(),
                                                                                  processed_df['Taken by' + ind],
                                                                                  np.NaN)
                                        processed_df['Report Number' + ind] = np.where(
                                            ~processed_df['Date_' + ind].isnull(),
                                            processed_df['Report Number' + ind],
                                            np.NaN)
                                        # processed_df = processed_df[processed_df['Feature Type'].str.strip().astype(bool)] #drop rows where FType is empty
                                    processed_df = processed_df[
                                        processed_df['Feature Type'].str.strip().astype(
                                            bool)]  # drop rows where FType is empty
                                    processed_df.drop(thickness_cols.columns, axis=1, inplace=True)
                                    processed_df.drop(processed_df.columns[[1, 2, 3, 4, 5, 6, 7, 8, 9]], axis=1,
                                                      inplace=True)
                                    try:
                                        processed_df.drop([''], axis=1,
                                                          inplace=True)  # remove column with empty string header. Was added automatically during dataframe processing
                                    except:
                                        pass

                                    # display(processed_df)
                                    df_concatenated_in_loop = df_concatenated_in_loop.append(processed_df)

                df_fill_unmatched_entries = DNO_loadsheet_filled_df_orig.merge(
                    df_concatenated_in_loop.drop_duplicates(), on=['Piping Tag/ Line Number*', 'Feature ID'],
                    how='right', indicator=True).query('_merge == "right_only"').drop('_merge', 1)
                #print(df_fill_unmatched_entries)
                replace_columns = dict(
                    zip(df_concatenated_in_loop.columns[2:], DNO_loadsheet_filled_df.columns[7:]))
                df_concatenated_in_loop.rename(columns=replace_columns, inplace=True)

                for col in df_concatenated_in_loop.columns:
                    try:
                        df_concatenated_in_loop[col] = df_concatenated_in_loop[col].astype(float)
                    except:
                        pass

                for col in DNO_loadsheet_filled_df.columns:
                    try:
                        DNO_loadsheet_filled_df[col] = DNO_loadsheet_filled_df[col].astype(float)
                    except:
                        pass

                # display(list(set(DNO_loadsheet_filled_df.columns.intersection(df_concatenated_in_loop.columns))))
                DNO_loadsheet_filled_df['Piping Tag/ Line Number*'] = DNO_loadsheet_filled_df[
                    'Piping Tag/ Line Number*'].replace("''", "\"", regex=True)
                DNO_loadsheet_filled_df = pd.merge(DNO_loadsheet_filled_df, df_concatenated_in_loop,
                                                   on=['Piping Tag/ Line Number*', 'Feature ID'], how='left')
                # TML_df = pd.merge(TML_df_to_fill, TML_df_QCed, on=['Feature ID'], how='left')
                # TML_df = (pd.concat([TML_df_to_fill, TML_df_QCed]).groupby(['Feature ID','Reading1 @ 0 deg',	'Reading1 @ 90 deg',	'Reading1 @ 180 deg',	'Reading1 @ 270 deg',	'Minimum WT1',	'Maximum WT1',	'Average WT1',	'Report Number',	'Comment/Remarks'])['Date 1* (YYYY-MM-DD)'].apply(list).reset_index())

                replace_x_with_y_and_drop_y_and_rename_headers(DNO_loadsheet_filled_df)
                # DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.replace('NaT',np.NaN, regex=True)
                # DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.replace('nan',np.NaN, regex=True)

                # DNO_loadsheet_filled_df.columns = DNO_loadsheet_filled_df_multiIndex.columns
                # DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.set_index(
                #    [('Circuit Tag', 'Automatically filled from piping loadsheet', 'Piping Tag/ Line Number*')]).rename_axis(index=None,
                #                                                                                                             columns=(
                #                                                                                                                 'Circuit Tag',
                #                                                                                                                 'Automatically filled from piping loadsheet',
                #                                                                                                                 'Piping Tag/ Line Number*'))

                DNO_loadsheet_filled_df.columns = DNO_loadsheet_filled_df_multiIndex.columns
                DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.set_index(
                    [('Circuit Tag', 'Automatically filled from piping loadsheet',
                      'Piping Tag/ Line Number*')]).rename_axis(
                    index=None,
                    columns=(
                        'Circuit Tag',
                        'Automatically filled from piping loadsheet',
                        'Piping Tag/ Line Number*'))

                DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.drop_duplicates()

                # writer = pd.ExcelWriter('circuit_level_DPE_IR_for_QC.xlsx', engine='xlsxwriter')
                # DPE_loadsheet_filled_df.to_excel(writer, sheet_name='Sheet1')
                # writer.sheets['Sheet1'].set_row(2, None, None, {'hidden': True})
                # writer.save()
                # print(DPE_loadsheet_filled_df.head())
                DNO_loadsheet_filled_df.to_excel(os.path.join(subdir, directory + '_for_QC.xlsx'))  # , engine='openpyxl')

                from openpyxl import Workbook, load_workbook
                from openpyxl.styles import Alignment

                wb = load_workbook(os.path.join(subdir, directory + '_for_QC.xlsx'))
                sheet = wb.active
                sheet.merge_cells('A1:C1')
                cell = sheet.cell(row=1, column=1)
                cell.value = 'Circuit Tag'

                sheet.merge_cells('A2:G2')
                cell = sheet.cell(row=2, column=1)
                cell.value = 'Automatically filled from piping loadsheet'

                cell.alignment = Alignment(horizontal='center', vertical='center')

                sheet.delete_rows(4)
                wb.save(os.path.join(subdir, directory + '_for_QC.xlsx'))

                end_time = time.time()
                path, dirs, files = next(os.walk(os.path.join(subdir)))
                file_count = len(files)

                print("\n Time taken to process the circuit", directory," with ",file_count,"Inspection Reports: ", end_time - start_time,"seconds\n")


else:
    for subdir, dirs, files in os.walk(rootdir):
        for directory in dirs:
            #print(subdir, directory)
            # print(dirs)
            # print(os.path.basename(subdir))
            if project_number == '1':
                print("Select the pre-populated loadsheet to fill with the extracted thickness readings for the circuit",
                      directory)
                # legend_file = input('')
                # legend_file = "legend_sheet.xlsx"

                Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
                DPE_loadsheet = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
                start_time = time.time()

                DPE_loadsheet_filled_df = pd.read_excel(DPE_loadsheet,
                                                        header=2)  # , engine='openpyxl')  # , engine='openpyxl')
                DPE_loadsheet_filled_df_multiIndex = pd.read_excel(DPE_loadsheet, header=[0, 1, 2])  # , engine='openpyxl')
                DPE_loadsheet_filled_df_orig = DPE_loadsheet_filled_df.copy()
                # for directory in dirs:
                # print(directory)
                df_concatenated_in_loop = pd.DataFrame()
                print("\nProcessing circuit ", directory, "\n")
                #print(os.path.join(subdir, directory))
                circuit_name = os.fsencode(os.path.join(subdir, directory))
                #print("Circuit name", circuit_name)
                for file in os.listdir(circuit_name):
                    file = os.fsdecode(file)
                    #print(file)
                    if file.endswith('.xls') or file.endswith('.xlsx') or file.endswith('.xlsm'):
                        if file.endswith('for_QC.xlsx') == False:# or file.endswith('validated.xlsx') == False:
                            try:
                                print("Processing report ", file)
                                test_df = pd.read_excel(os.path.join(subdir, directory, file))

                                test_df = test_df.replace('\n', ' ', regex=True)
                                test_df = test_df.replace(np.nan, ' ', regex=True)
                                # display(test_df)

                                try:
                                    line_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'line_number_str1')).any(level=0)]
                                    # print(line_number)
                                    line_number = str(line_number.apply(''.join, axis=1))
                                    line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                    # line_number = re.search('(?<=LINE NO. )(\w[\w-]+)', line_number).group()
                                    line_number = re.search('(?<=' + re.escape(config.get('string_match', 'line_number_str1')) + ' )[^.\s]*',
                                                            line_number).group()
                                    # line_number = re.search('(?<=LINE NO. )[^.\s]*', line_number).group()
                                    # print("Line number: ", line_number)
                                except:
                                    line_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'line_number_str2')).any(level=0)]
                                    # print(line_number)
                                    line_number = str(line_number.apply(''.join, axis=1))
                                    line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                    # line_number = re.search('(?<=LINE NO )(\w[\w-]+)', line_number).group()
                                    line_number = re.search('(?<=' + re.escape(config.get('string_match', 'line_number_str2')) + ' )[^.\s]*',
                                                            line_number).group()
                                    # line_number = re.search('(?<=LINE NO )[^.\s]*', line_number).group()
                                    # print("Line number: ", line_number)

                                try:
                                    rep_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'report_number_str1')).any(level=0)]
                                    rep_number = str(rep_number.apply(''.join, axis=1))
                                    rep_number = ' '.join(rep_number.split())  # remove extra whitespaces >1
                                    rep_number = re.search('(?<=' + re.escape(config.get('string_match', 'report_number_str1')) + ' )[^.\s]*',
                                                           rep_number).group()
                                    # print("Report number: ", rep_number)
                                except:
                                    rep_number = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'report_number_str2')).any(level=0)]
                                    rep_number = str(rep_number.apply(''.join, axis=1))
                                    rep_number = ' '.join(rep_number.split())  # remove extra whitespaces >1
                                    rep_number = re.search('(?<=' + re.escape(config.get('string_match', 'report_number_str2')) + ' )[^.\s]*',
                                                           rep_number).group()
                                    # print("Report number: ", rep_number)

                                try:
                                    taken_by_copy = ''
                                    taken_by_copy = test_df.loc[
                                        test_df.stack().str.contains(config.get('string_match', 'taken_by_start_str1')).any(level=0)]
                                    taken_by_copy = str(taken_by_copy.apply(''.join, axis=1))
                                    taken_by_copy = ' '.join(taken_by_copy.split())  # remove extra whitespaces
                                    start_pts = [i + len(config.get('string_match', 'taken_by_start_str1')) for i in range(len(taken_by_copy)) if
                                                 taken_by_copy.startswith(config.get('string_match', 'taken_by_start_str1'), i)]
                                    end_pts = [i for i in range(len(taken_by_copy)) if
                                               taken_by_copy.startswith(config.get('string_match', 'taken_by_end_str1'), i)]
                                    taken_by = ''
                                    for i in range(len(start_pts)):
                                        taken_by = taken_by + taken_by_copy[start_pts[i]:end_pts[i]] + ','
                                    # taken_by = re.search('(?<=INSPECTION TECHNICIAN )(\w[\w-]+)',taken_by).group()
                                    taken_by = taken_by[:-2]  # remove last comma appended
                                    # print("Taken by: ", taken_by)
                                except:
                                    taken_by = None

                                try:
                                    date = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'date_str1')).any(level=0)].to_string(
                                        header=False, index=False)
                                    # date = date.apply(''.join, axis=1)
                                    date = ' '.join(date.split())  # remove extra whitespaces >1
                                    # print(date)

                                    # date = re.search('(?<=DATE OF INSPECTION )(\w[\w-]+)',date).group()
                                    # print("Date js: ", date)

                                    """
                                    date = date.replace('\n','')
                                    date = date.replace('\\','')
                                    date = date.replace('Unnamed:','')
                                    date = date.replace(':','')
                                    #date = date.replace('-','/')
                                    date = ' '.join(date.split())
                                    #date = dparser.parse(str(date),fuzzy=True).strftime("%Y-%m-%d")
                                    """
                                    date = date.partition(config.get('string_match', 'date_str1'))[2]
                                    date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                    # print(date)

                                    # date = datetime.strptime(date.group(), '%Y-%m-%d').date()
                                    # date = parse_datetime_remove_useless_end(date)
                                    # print("Date: ", date)
                                except:
                                    from _datetime import datetime

                                    date = test_df.loc[test_df.stack().str.contains(config.get('string_match', 'date_str2')).any(level=0)].to_string(
                                        header=False, index=False)
                                    date = ' '.join(date.split())  # remove extra whitespaces >1
                                    date = date.partition(config.get('string_match', 'date_str2'))[2]
                                    # date = re.search('(?<=INSPECTION DATE)(\w[\w-]+)',date).group()
                                    # print("Date is: ", date)
                                    date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")

                                    # date = datetime.strptime(date.group(), '%Y-%m-%d').date()
                                    # date = parse_datetime_remove_useless_end(date)
                                    # print("Date: ", date)
                                    # print(date)

                                # header_row_index = test_df.index[test_df.stack().str.contains('t-ALERT').any(level=0)].to_list()[0]#.index
                                header_row_index = \
                                    test_df.index[
                                        test_df.stack().str.contains(config.get('header_match', 'header_row_match_str1')).any(level=0)].to_list()[
                                        0]  # .index
                                test_df.columns = test_df.iloc[header_row_index]

                                processed_df = test_df.iloc[header_row_index + 1:].reset_index(drop=True)

                                processed_df.rename(columns={header_row_index: None, config.get('header_match', 'feature_type_str1'): 'Feature Type',
                                                             config.get('header_match', 'feature_ID_str1'): 'Feature ID',
                                                             config.get('header_match', 'feature_size_str1'): 'Feature Size',
                                                             config.get('header_match', 'minimum_WT_str1'): 'Minimum WT', config.get('header_match', 'maximum_WT_str1'): 'MaximumWT',
                                                             config.get('header_match', 'nwt_str1'): 'NWT, mm',
                                                             config.get('header_match', 'header_row_match_str1'): 'Comment/\nRemarks'}, inplace=True)
                                processed_df['Date 1*\n(YYYY-MM-DD)'] = date
                                processed_df['Piping Tag/ Line Number*'] = line_number
                                processed_df['Report Number'] = rep_number
                                processed_df['Taken by'] = taken_by

                                processed_df['Average WT'] = (pd.to_numeric(processed_df['Minimum WT'],
                                                                            errors='coerce') + pd.to_numeric(
                                    processed_df['MaximumWT'],
                                    errors='coerce')) / 2

                                processed_df['Comment/\nRemarks'] = 'Inspection Findings: ' + processed_df[
                                    'Comment/\nRemarks'].map(
                                    str) + '\nRecommended Action: ' + processed_df['RECOMMENDED ACTION'].map(str)
                                processed_df.drop([config.get('header_match', 'loss_str1'), config.get('header_match', 'recommended_action_str1'), config.get('header_match', 'photo_str1')], axis=1, inplace=True)

                                # processed_df.columns = ['Feature ID',	'Feature Type', 'Feature Size', 't-ANOMALY',	'Minimum WT',	'Maximum WT',	'Average WT', 't-ALERT',	'LOSS',	'Comment/Remarks', 'RECOMMENDED ACTION', 'PHOTO']
                                processed_df = processed_df[
                                    ['Piping Tag/ Line Number*', 'Feature Type', 'Feature ID', 'Feature Size', 'NWT, mm',
                                     'Date 1*\n(YYYY-MM-DD)', 'Minimum WT', 'MaximumWT', 'Average WT', 'Report Number',
                                     'Taken by',
                                     'Comment/\nRemarks']]

                                df_concatenated_in_loop = df_concatenated_in_loop.append(processed_df)
                                print(processed_df)
                            except:
                                pass

                                # display(processed_df.columns)
                # display(processed_df['Feature Size'].to_string(index=False))

                # print(DPE_loadsheet_filled_df)
                # print(df_concatenated_in_loop)
                # df_concatenated_in_loop = df_concatenated_in_loop.replace(' ',np.NaN, regex=True)
                df_concatenated_in_loop = df_concatenated_in_loop[df_concatenated_in_loop['Feature ID'] != ' ']
                # df_concatenated_in_loop['Piping Tag/ Line Number*'] = df_concatenated_in_loop['Piping Tag/ Line Number*'].replace('#','x',regex=True)
                # df_concatenated_in_loop.to_excel("df_concatenated_in_loop.xlsx", index=False)

                matched_entries = pd.merge(DPE_loadsheet_filled_df, df_concatenated_in_loop,
                                           on=['Piping Tag/ Line Number*', 'Feature ID'], how='inner',
                                           right_index=True)  # ,  indicator=True)
                unmatched_entries = pd.merge(DPE_loadsheet_filled_df, matched_entries,
                                             on=['Piping Tag/ Line Number*', 'Feature ID'],
                                             how='left', indicator=True).query('_merge != "both"').drop('_merge', 1)

                replace_x_with_y_and_drop_y_and_rename_headers(unmatched_entries)
                replace_x_with_y_and_drop_y_and_rename_headers(matched_entries)

                unmatched_entries = unmatched_entries.iloc[:, :-10]
                # print(matched_entries)
                DPE_loadsheet_filled_df = pd.concat([unmatched_entries, matched_entries],
                                                    join='outer')  # .reset_index(drop=True)

                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.drop_duplicates()
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.sort_values(by="Date 1*\n(YYYY-MM-DD)",
                                                                              key=pd.to_datetime, ascending=False)

                # DPE_loadsheet_filled_df['Date 1*\n(YYYY-MM-DD)'] = pd.to_datetime(DPE_loadsheet_filled_df['Date 1*\n(YYYY-MM-DD)'], format='%Y-%m-%d').dt.date
                # DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.sort_values(by="Date 1*\n(YYYY-MM-DD)", ascending=False)

                # DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.groupby('Feature ID')
                # print(DPE_loadsheet_filled_df)

                # filter entries having multiple year readings
                grouped_df = DPE_loadsheet_filled_df.groupby(level=0).filter(lambda x: len(x) > 1)  # .transform('count') > 1].dropna(axis = 0, how = 'all').reset_index()#.shift(19, axis=1)#.apply(lambda x: ','.join(x.astype(str)))
                # grouped_df = DPE_loadsheet_filled_df.groupby(['Piping Tag/ Line Number*', 'Feature ID']).filter(lambda x: len(x) > 1)
                # print(grouped_df)
                # grouped_df_rows = grouped_df.groupby(['Feature ID'])

                grouped_df['Group'] = grouped_df.groupby(['Piping Tag/ Line Number*', 'Feature ID']).cumcount()
                # print(grouped_df)
                dfs = dict(tuple(grouped_df.groupby('Group')))

                incr_index = 12
                start = 7
                interval_list = []

                for i in range(start, len(grouped_df.columns), incr_index):
                    interval_list.append(i)

                counter2 = 0
                grouped_dfs_concat = pd.DataFrame()


                def generate_df_from_grouped(dfs, index):
                    # dfs_0=None
                    counter = 0
                    for i in range(len(dfs)):

                        # print("i=",i,"counter=",counter)
                        # display(dfs[i])
                        if counter < len(dfs):
                            try:
                                globals()['dfs_{0}'.format(i)] = dfs[i].iloc[index].reset_index().set_index('index').T
                            except:
                                pass
                            counter += 1

                    # global dfs_0

                    for intr, ind in zip(range(len(interval_list)), range(len(dfs))):
                        # print(intr,ind)
                        try:
                            dfs_0.iloc[:, interval_list[intr]:interval_list[intr + 1]] = globals()[
                                                                                             'dfs_{0}'.format(ind)].iloc[:,
                                                                                         start:start + incr_index].values
                        except:
                            pass
                        # print(interval_list[intr])
                    # display(globals()['dfs_{0}'.format(index)])
                    globals()['dfs_{0}'.format(i)] = None

                    return (dfs_0)

                    # dfs_0=None


                for counter2 in range(len(grouped_df['Group'])):
                    # print("Counter2=",counter2)

                    dfs_received = generate_df_from_grouped(dfs, counter2)

                    grouped_dfs_concat = grouped_dfs_concat.append(dfs_received)
                    # dfs_0=None
                try:
                    del grouped_dfs_concat['Group']
                except:
                    pass

                grouped_dfs_concat.index
                # print(grouped_dfs_concat)
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df[
                    ~DPE_loadsheet_filled_df.index.isin(grouped_dfs_concat.index)]
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.append(grouped_dfs_concat)
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.sort_index(axis=0, ascending=True)
                # print(DPE_loadsheet_filled_df.head())

                df_concatenated_in_loop_new = df_concatenated_in_loop.copy()
                df_concatenated_in_loop_new = df_concatenated_in_loop_new[
                    (df_concatenated_in_loop_new['Feature Type'] != 'MIN') & (
                                df_concatenated_in_loop_new['Feature Type'] != 'MAX') & (
                                df_concatenated_in_loop_new['Feature Type'] != ' ')]
                df_concatenated_in_loop_new['Feature ID'] = df_concatenated_in_loop_new['Feature ID'].astype(str)
                DPE_loadsheet_filled_df_orig['Feature ID'] = DPE_loadsheet_filled_df_orig['Feature ID'].astype(str)

                df_fill_unmatched_entries = DPE_loadsheet_filled_df_orig.merge(
                    df_concatenated_in_loop_new.drop_duplicates(), on=['Piping Tag/ Line Number*', 'Feature ID'],
                    how='right', indicator=True).query('_merge == "right_only"').drop('_merge', 1)
                replace_x_with_y_and_drop_y_and_rename_headers(df_fill_unmatched_entries)
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.append(df_fill_unmatched_entries)

                DPE_loadsheet_filled_df.columns = DPE_loadsheet_filled_df_multiIndex.columns
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.set_index(
                    [(
                     'Circuit Tag', 'Automatically filled from piping loadsheet', 'Piping Tag/ Line Number*')]).rename_axis(
                    index=None,
                    columns=(
                        'Circuit Tag',
                        'Automatically filled from piping loadsheet',
                        'Piping Tag/ Line Number*'))
                DPE_loadsheet_filled_df = DPE_loadsheet_filled_df.drop_duplicates()

                # writer = pd.ExcelWriter('circuit_level_DPE_IR_for_QC.xlsx', engine='xlsxwriter')
                # DPE_loadsheet_filled_df.to_excel(writer, sheet_name='Sheet1')
                # writer.sheets['Sheet1'].set_row(2, None, None, {'hidden': True})
                # writer.save()
                # print(DPE_loadsheet_filled_df.head())

                DPE_loadsheet_filled_df.to_excel(
                        os.path.join(subdir, directory, directory + '_for_QC.xlsx'))  # , engine='openpyxl')

                from openpyxl import Workbook, load_workbook
                from openpyxl.styles import Alignment

                wb = load_workbook(os.path.join(subdir, directory, directory + '_for_QC.xlsx'))
                sheet = wb.active
                sheet.merge_cells('A1:C1')
                cell = sheet.cell(row=1, column=1)
                cell.value = 'Circuit Tag'

                sheet.merge_cells('A2:G2')
                cell = sheet.cell(row=2, column=1)
                cell.value = 'Automatically filled from piping loadsheet'

                cell.alignment = Alignment(horizontal='center', vertical='center')

                sheet.delete_rows(4)
                wb.save(os.path.join(subdir, directory, directory + '_for_QC.xlsx'))

                end_time = time.time()
                path, dirs, files = next(os.walk(os.path.join(subdir, directory)))
                file_count = len(files)

                print("\n Time taken to process the circuit", directory, " with ", file_count, "Inspection Reports: ",
                      end_time - start_time, "seconds\n")

            if project_number == '2':
                print(
                    "Select the pre-populated loadsheet to fill with the extracted thickness readings for the circuit", directory)
                # legend_file = input('')
                # legend_file = "legend_sheet.xlsx"

                Tk().withdraw()  # we don't want a full GUI, so keep the root window from appearing
                DNO_loadsheet = askopenfilename()  # show an "Open" dialog box and return the path to the selected file
                start_time = time.time()

                DNO_loadsheet_filled_df = pd.read_excel(DNO_loadsheet,
                                                        header=2)  # , engine='openpyxl')  # , engine='openpyxl')
                DNO_loadsheet_filled_df_multiIndex = pd.read_excel(DNO_loadsheet,
                                                                   header=[0, 1, 2])  # , engine='openpyxl')
                DNO_loadsheet_filled_df_orig = DNO_loadsheet_filled_df.copy()
                df_concatenated_in_loop = pd.DataFrame()
                print("\nProcessing circuit ", directory, "\n")
                circuit_name = os.fsencode(os.path.join(subdir, directory))
                pattern = r'(?i)(.*Thickness.*)'
                for file in os.listdir(circuit_name):
                    file = os.fsdecode(file)

                    if file.endswith('for_QC.xlsx') == False:
                        #print(file)
                        if file.endswith('.xls') or file.endswith('.XLS') or file.endswith('.xlsx') or file.endswith('.xlsm'):
                          # or file.endswith('validated.xlsx') == False:
                            print("Processing report:", file)
                            test_df = pd.ExcelFile(os.path.join(subdir, directory, file))



                            # print("sheetNames",test_df.sheet_names)
                            for sheet in test_df.sheet_names:
                                if re.search(pattern, sheet):  # when matching pattern add the dataframe to the list
                                    # print(file)
                                    test_df = pd.read_excel(os.path.join(subdir, directory, file), sheet_name=sheet)
                                    test_df = test_df.replace('\n', ' ', regex=True)
                                    test_df = test_df.replace(np.nan, ' ', regex=True)
                                    # display(test_df)

                                    try:
                                        line_number = test_df.loc[
                                            test_df.stack().str.contains(
                                                config.get('string_match_DNO', 'line_number_str1')).any(
                                                level=0)]
                                        # print(line_number)
                                        line_number = str(line_number.apply(''.join, axis=1))
                                        line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                        # print("Line number: ", line_number)
                                        # print(line_number.split(config.get('string_match', 'line_number_str3'))[1])
                                        # line_number = re.search('(?<=Line No: )(\w[\w-]+)', line_number).group()
                                        line_number = re.search(
                                            '(?<=' + re.escape(
                                                config.get('string_match_DNO', 'line_number_str1')) + ' )[^.\s]*',
                                            line_number).group()
                                        # line_number = re.search('(?<=LINE NO. )[^.\s]*', line_number).group()
                                        # print("Line number: ", line_number)
                                    except:
                                        line_number = test_df.loc[
                                            test_df.stack().str.contains(
                                                config.get('string_match_DNO', 'line_number_str2')).any(
                                                level=0)]
                                        # print(line_number)
                                        line_number = str(line_number.apply(''.join, axis=1))
                                        line_number = ' '.join(line_number.split())  # remove extra whitespaces >1
                                        # print("Line number: ", line_number)
                                        # print(line_number.split(config.get('string_match', 'line_number_str3'))[1])
                                        # line_number = re.search('(?<=Line No: )(\w[\w-]+)', line_number).group()
                                        line_number = re.search(
                                            '(?<=' + re.escape(
                                                config.get('string_match_DNO', 'line_number_str2')) + ' )[^.\s]*',
                                            line_number).group()
                                        # line_number = re.search('(?<=LINE NO. )[^.\s]*', line_number).group()
                                        # print("Line number: ", line_number)

                                    rep_number = test_df.loc[
                                        test_df.stack().str.contains(
                                            config.get('string_match_DNO', 'report_number_str1')).any(
                                            level=0)]
                                    rep_number = str(rep_number.apply(''.join, axis=1))
                                    rep_number = ' '.join(rep_number.split())  # remove extra whitespaces >1
                                    rep_number = re.search(
                                        '(?<=' + re.escape(
                                            config.get('string_match_DNO', 'report_number_str1')) + ' )[^.\s]*',
                                        rep_number).group()
                                    # print("rep_number: ", rep_number)

                                    try:
                                        taken_by_copy = ''
                                        taken_by_copy = test_df.loc[
                                            test_df.stack().str.contains(
                                                config.get('string_match_DNO', 'taken_by_start_str1')).any(
                                                level=0)]
                                        taken_by_copy = str(taken_by_copy.apply(''.join, axis=1))
                                        taken_by_copy = ' '.join(taken_by_copy.split())  # remove extra whitespaces
                                        start_pts = [i + len(config.get('string_match_DNO', 'taken_by_start_str1')) for
                                                     i in
                                                     range(len(taken_by_copy)) if
                                                     taken_by_copy.startswith(
                                                         config.get('string_match_DNO', 'taken_by_start_str1'),
                                                         i)]
                                        end_pts = [i for i in range(len(taken_by_copy)) if
                                                   taken_by_copy.startswith(
                                                       config.get('string_match_DNO', 'taken_by_end_str1'), i)]
                                        taken_by = ''
                                        for i in range(len(start_pts)):
                                            taken_by = taken_by + taken_by_copy[start_pts[i]:end_pts[i]] + ','
                                        # taken_by = re.search('(?<=INSPECTION TECHNICIAN )(\w[\w-]+)',taken_by).group()
                                        taken_by = taken_by[:-2]  # remove last comma appended
                                        # print("Taken by: ", taken_by)
                                    except:
                                        taken_by = None

                                    """
                                    date = test_df.loc[test_df.stack().str.contains(config.get('string_match_DNO', 'date_str1')).any(level=0)]
                                    date = str(date.apply(''.join, axis=1))
                                    date = ' '.join(date.split())  # remove extra whitespaces >1
                                    date = re.search('(?<='+re.escape(config.get('string_match_DNO', 'date_str1'))+' )[^.\s]*', date).group()
                                    date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                    print("Date:", date)
                                    """
                                    try:
                                        header_row_index = test_df.index[test_df.stack().str.contains(
                                            config.get('header_match_DNO', 'header_row_match_str1')).any(
                                            level=0)].to_list()[
                                            0]  # .index
                                        header_row_str = config.get('header_match_DNO', 'header_row_match_str1')
                                    except:
                                        header_row_index = test_df.index[test_df.stack().str.contains(
                                            config.get('header_match_DNO', 'header_row_match_str2')).any(
                                            level=0)].to_list()[
                                            0]  # .index
                                        header_row_str = config.get('header_match_DNO', 'header_row_match_str2')
                                    test_df.columns = test_df.iloc[header_row_index]

                                    for col in test_df.columns:
                                        test_df.rename(columns={col: ' '.join(col.split())},
                                                       inplace=True)  # remove extra whitespaces in column names
                                    processed_df = test_df.iloc[header_row_index + 1:].reset_index(drop=True)
                                    processed_df.rename(columns={header_row_index: None, config.get('header_match_DNO',
                                                                                                    'feature_type_str1'): 'Feature Type',
                                                                 config.get('header_match_DNO',
                                                                            'feature_ID_str1'): 'Feature ID',
                                                                 header_row_str: 'Feature Size',
                                                                 config.get('header_match_DNO',
                                                                            'remarks_str1'): 'Comment/\nRemarks'},
                                                        inplace=True)
                                    processed_df['Feature ID'] = processed_df['Feature ID'].str.strip()
                                    processed_df['Feature ID'] = processed_df['Feature ID'].str.split("-", expand=True)[
                                        0]
                                    processed_df['Feature ID'] = processed_df['Feature ID'].str.strip()
                                    processed_df["Comment/\nRemarks"] = processed_df["Comment/\nRemarks"].replace('"',
                                                                                                                  '',
                                                                                                                  regex=True)

                                    thickness_cols = processed_df.filter(like='Thickness', axis=1)
                                    # print(thickness_cols.columns)
                                    # processed_df[col] = processed_df[col].apply(lambda x: [i for i in x if i == i])

                                    for col in thickness_cols:
                                        processed_df[col] = pd.to_numeric(processed_df[col], errors='coerce')

                                    # display(processed_df)
                                    processed_df = processed_df.groupby('Feature ID', as_index=False).agg(
                                        lambda x: x.tolist())
                                    # display(processed_df)
                                    processed_df['Piping Tag/ Line Number*'] = line_number
                                    processed_df['Piping Tag/ Line Number*'] = processed_df[
                                        'Piping Tag/ Line Number*'].replace(
                                        "''", "\"", regex=True)
                                    processed_df["Feature Type"] = processed_df['Feature Type'].map(np.unique)
                                    processed_df["Feature Size"] = processed_df['Feature Size'].map(np.unique)
                                    processed_df["Comment/\nRemarks"] = processed_df['Comment/\nRemarks'].map(np.unique)

                                    processed_df["Feature Type"] = processed_df['Feature Type'].str[
                                        0]  # remove square braces
                                    processed_df["Feature Size"] = processed_df['Feature Size'].str[0]
                                    processed_df["Comment/\nRemarks"] = processed_df['Comment/\nRemarks'].apply(
                                        lambda x: ','.join(map(str, x)))
                                    processed_df["Comment/\nRemarks"] = processed_df["Comment/\nRemarks"].replace(',',
                                                                                                                  '',
                                                                                                                  regex=True)
                                    # processed_df['Comment/\nRemarks'] = processed_df['Comment/\nRemarks'].str.replace('"','')
                                    # processed_df["Comment/\nRemarks"]=processed_df['Comment/\nRemarks']

                                    thickness_cols = processed_df.filter(like='Thickness', axis=1)
                                    thickness_cols_names = thickness_cols.columns
                                    for col in thickness_cols.columns:
                                        thickness_cols[col] = thickness_cols[col].apply(
                                            lambda x: [i for i in x if i == i]).apply(
                                            lambda y: np.nan if len(
                                                y) == 0 else y)  # two apply methods. First to remove all nan's from list. This results in empty list. Second apply to replace empty list with NaN
                                        date = ''.join(col)
                                        date = ' '.join(date.split())  # remove extra whitespaces >1
                                        # date = re.search('(?<='+re.escape(config.get('string_match_DNO', 'date_str1'))+' )[^.\s]*', date).group()
                                        date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                        for val in thickness_cols[col]:

                                            if val is not np.nan:
                                                val = val.append(date)
                                    # display(thickness_cols)
                                    # date_cols = pd.concat([v.assign(date_col=k) for k, v in thickness_cols.groupby(axis=1,level=0,sort=False)],axis=1)
                                    # display(date_cols)
                                    thickness_cols = thickness_cols.apply(lambda x: pd.Series(x.dropna().values),
                                                                          axis=1)  # move nans to the end and make the readings to be continuous among dates
                                    # display(shifted_df)
                                    thickness_cols.columns = thickness_cols_names
                                    # display(thickness_cols)

                                    for ind, col in zip(range(0, len(thickness_cols.columns), 1),
                                                        thickness_cols.columns):
                                        processed_df[col] = thickness_cols[col]
                                        processed_df[col] = [[np.NaN, np.NaN, np.NaN, np.NaN] if x is np.NaN else x for
                                                             x in
                                                             processed_df[
                                                                 col]]  # replace NaN with a list of four nan's as earlier
                                        # display(thickness_cols[col])

                                        #print("index", ind)
                                        ind = str(ind)
                                        """
                                        date = ''.join(col)
                                        date = ' '.join(date.split())  # remove extra whitespaces >1
                                        #date = re.search('(?<='+re.escape(config.get('string_match_DNO', 'date_str1'))+' )[^.\s]*', date).group()
                                        date = dparser.parse(date, fuzzy=True).strftime("%Y-%m-%d")
                                        #print("Date:", date)

                                        #processed_df[date+'Date 1*\n(YYYY-MM-DD)'] = date

                                        #processed_df[col] = processed_df[col].apply(lambda d: np.nan if d is np.nan else col+processed_df[col].astype(str)) #col+thickness_cols[col].astype(str)
                                        processed_df['Date_'+ind] = date
                                        """
                                        processed_df['Date_' + ind] = processed_df[col].str[-1]
                                        # display(pd.DataFrame(processed_df[col].values.tolist()))
                                        for i in processed_df[col]:
                                            i.pop(-1)
                                        processed_df = processed_df.join(
                                            pd.DataFrame(processed_df[col].values.tolist()).add_prefix(
                                                'Reading_@_' + ind))

                                        # display(processed_df)
                                        processed_df['Min_WT' + ind] = [min(x) for x in processed_df[col].tolist()]
                                        processed_df['Max_WT' + ind] = [max(x) for x in processed_df[col].tolist()]
                                        # df['Avg_WT_1'] = [sum([ float(n) for n in x ])/len(x) for x in df.Thickness_1.tolist()]
                                        # processed_df['Avg_WT_1'] = [np.array(x).astype(np.float).mean() for x in processed_df[col].values]

                                        processed_df['Avg_WT' + ind] = [sum([float(n) for n in x]) / len(x) for x in
                                                                        processed_df[col].tolist()]
                                        # display(processed_df)
                                        processed_df['Report Number' + ind] = rep_number
                                        processed_df['Taken by' + ind] = taken_by
                                        processed_df['Comment/\nRemarks' + ind] = processed_df['Comment/\nRemarks']
                                        processed_df['Probe ID' + ind] = ''
                                        processed_df['Taken by' + ind] = np.where(~processed_df['Date_' + ind].isnull(),
                                                                                  processed_df['Taken by' + ind],
                                                                                  np.NaN)
                                        processed_df['Report Number' + ind] = np.where(
                                            ~processed_df['Date_' + ind].isnull(),
                                            processed_df['Report Number' + ind],
                                            np.NaN)
                                        # processed_df = processed_df[processed_df['Feature Type'].str.strip().astype(bool)] #drop rows where FType is empty
                                    processed_df = processed_df[
                                        processed_df['Feature Type'].str.strip().astype(
                                            bool)]  # drop rows where FType is empty
                                    processed_df.drop(thickness_cols.columns, axis=1, inplace=True)
                                    processed_df.drop(processed_df.columns[[1, 2, 3, 4, 5, 6, 7, 8, 9]], axis=1,
                                                      inplace=True)
                                    try:
                                        processed_df.drop([''], axis=1,
                                                          inplace=True)  # remove column with empty string header. Was added automatically during dataframe processing
                                    except:
                                        pass

                                    # display(processed_df)
                                    df_concatenated_in_loop = df_concatenated_in_loop.append(processed_df)

                df_fill_unmatched_entries = DNO_loadsheet_filled_df_orig.merge(
                    df_concatenated_in_loop.drop_duplicates(), on=['Piping Tag/ Line Number*', 'Feature ID'],
                    how='right', indicator=True).query('_merge == "right_only"').drop('_merge', 1)
                #print(df_fill_unmatched_entries)
                replace_columns = dict(
                    zip(df_concatenated_in_loop.columns[2:], DNO_loadsheet_filled_df.columns[7:]))
                df_concatenated_in_loop.rename(columns=replace_columns, inplace=True)

                for col in df_concatenated_in_loop.columns:
                    try:
                        df_concatenated_in_loop[col] = df_concatenated_in_loop[col].astype(float)
                    except:
                        pass

                for col in DNO_loadsheet_filled_df.columns:
                    try:
                        DNO_loadsheet_filled_df[col] = DNO_loadsheet_filled_df[col].astype(float)
                    except:
                        pass

                # display(list(set(DNO_loadsheet_filled_df.columns.intersection(df_concatenated_in_loop.columns))))
                DNO_loadsheet_filled_df['Piping Tag/ Line Number*'] = DNO_loadsheet_filled_df[
                    'Piping Tag/ Line Number*'].replace("''", "\"", regex=True)
                DNO_loadsheet_filled_df = pd.merge(DNO_loadsheet_filled_df, df_concatenated_in_loop,
                                                   on=['Piping Tag/ Line Number*', 'Feature ID'], how='left')
                # TML_df = pd.merge(TML_df_to_fill, TML_df_QCed, on=['Feature ID'], how='left')
                # TML_df = (pd.concat([TML_df_to_fill, TML_df_QCed]).groupby(['Feature ID','Reading1 @ 0 deg',	'Reading1 @ 90 deg',	'Reading1 @ 180 deg',	'Reading1 @ 270 deg',	'Minimum WT1',	'Maximum WT1',	'Average WT1',	'Report Number',	'Comment/Remarks'])['Date 1* (YYYY-MM-DD)'].apply(list).reset_index())

                replace_x_with_y_and_drop_y_and_rename_headers(DNO_loadsheet_filled_df)
                # DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.replace('NaT',np.NaN, regex=True)
                # DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.replace('nan',np.NaN, regex=True)

                # DNO_loadsheet_filled_df.columns = DNO_loadsheet_filled_df_multiIndex.columns
                # DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.set_index(
                #    [('Circuit Tag', 'Automatically filled from piping loadsheet', 'Piping Tag/ Line Number*')]).rename_axis(index=None,
                #                                                                                                             columns=(
                #                                                                                                                 'Circuit Tag',
                #                                                                                                                 'Automatically filled from piping loadsheet',
                #                                                                                                                 'Piping Tag/ Line Number*'))

                DNO_loadsheet_filled_df.columns = DNO_loadsheet_filled_df_multiIndex.columns
                DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.set_index(
                    [('Circuit Tag', 'Automatically filled from piping loadsheet',
                      'Piping Tag/ Line Number*')]).rename_axis(
                    index=None,
                    columns=(
                        'Circuit Tag',
                        'Automatically filled from piping loadsheet',
                        'Piping Tag/ Line Number*'))

                DNO_loadsheet_filled_df = DNO_loadsheet_filled_df.drop_duplicates()

                # writer = pd.ExcelWriter('circuit_level_DPE_IR_for_QC.xlsx', engine='xlsxwriter')
                # DPE_loadsheet_filled_df.to_excel(writer, sheet_name='Sheet1')
                # writer.sheets['Sheet1'].set_row(2, None, None, {'hidden': True})
                # writer.save()
                # print(DPE_loadsheet_filled_df.head())
                DNO_loadsheet_filled_df.to_excel(os.path.join(subdir, directory, directory + '_for_QC.xlsx'))  # , engine='openpyxl')

                from openpyxl import Workbook, load_workbook
                from openpyxl.styles import Alignment

                wb = load_workbook(os.path.join(subdir, directory, directory + '_for_QC.xlsx'))
                sheet = wb.active
                sheet.merge_cells('A1:C1')
                cell = sheet.cell(row=1, column=1)
                cell.value = 'Circuit Tag'

                sheet.merge_cells('A2:G2')
                cell = sheet.cell(row=2, column=1)
                cell.value = 'Automatically filled from piping loadsheet'

                cell.alignment = Alignment(horizontal='center', vertical='center')

                sheet.delete_rows(4)
                wb.save(os.path.join(subdir, directory, directory + '_for_QC.xlsx'))

                end_time = time.time()
                path, dirs, files = next(os.walk(os.path.join(subdir, directory)))
                file_count = len(files)

                print("\n Time taken to process the circuit", directory," with ",file_count,"Inspection Reports: ", end_time - start_time,"seconds\n")

def highlight(x):
    # r = 'red'
    # g = 'gray'
    # m1 = TML_df[deg0_reading_cols[1]] - TML_df[deg0_reading_cols[0]] <= -0.05
    # m1 = x['B'] > x['C']
    # m2 = x['D'] > x['E']

    df1 = pd.DataFrame('background-color: ', index=x.index, columns=x.columns)
    # rewrite values by boolean masks
    # select all values to default value - Transparent color
    df1.loc[:, :] = 'background-color: None'

    for i in range(len(min_WT_reading_cols)):
        # validation check for  NWT readings

        df1[deg0_reading_cols[i]] = np.where(
            (df1[deg0_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[deg0_reading_cols[i]] = np.where((TML_df[deg0_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                    TML_df[deg0_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                         TML_df[deg0_reading_cols[i]] > TML_df['CONC, mm']),
                                             'background-color: {}'.format('#32CD32'),
                                             'background-color: {}'.format('#FF6347'))



        df1[deg90_reading_cols[i]] = np.where(
            (df1[deg90_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[deg90_reading_cols[i]] = np.where((TML_df[deg90_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                    TML_df[deg90_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                          TML_df[deg90_reading_cols[i]] > TML_df['CONC, mm']),
                                              'background-color: {}'.format('#32CD32'),
                                              'background-color: {}'.format('#FF6347'))


        df1[deg180_reading_cols[i]] = np.where(
            (df1[deg180_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[deg180_reading_cols[i]] = np.where((TML_df[deg180_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                TML_df[deg180_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                       TML_df[deg180_reading_cols[i]] > TML_df['CONC, mm']),
                                               'background-color: {}'.format('#32CD32'),
                                               'background-color: {}'.format('#FF6347'))


        df1[deg270_reading_cols[i]] = np.where(
            (df1[deg270_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[deg270_reading_cols[i]] = np.where((TML_df[deg270_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                TML_df[deg270_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                       TML_df[deg270_reading_cols[i]] > TML_df['CONC, mm']),
                                               'background-color: {}'.format('#32CD32'),
                                               'background-color: {}'.format('#FF6347'))


        df1[min_WT_reading_cols[i]] = np.where(
            (df1[min_WT_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[min_WT_reading_cols[i]] = np.where((TML_df[min_WT_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                TML_df[min_WT_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                       TML_df[min_WT_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                               'background-color: {}'.format('#32CD32'),
                                               'background-color: {}'.format('#FF6347'))


        df1[max_WT_reading_cols[i]] = np.where(
            (df1[max_WT_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[max_WT_reading_cols[i]] = np.where((TML_df[max_WT_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                TML_df[max_WT_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                       TML_df[max_WT_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                               'background-color: {}'.format('#32CD32'),
                                               'background-color: {}'.format('#FF6347'))


        df1[avg_WT_reading_cols[i]] = np.where(
            (df1[avg_WT_reading_cols[i]].isna()), 'background-color: {}'.format('#FF6347'),
            'background-color: {}'.format('#32CD32'))
        df1[avg_WT_reading_cols[i]] = np.where((TML_df[avg_WT_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                TML_df[avg_WT_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                       TML_df[avg_WT_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                               'background-color: {}'.format('#32CD32'),
                                               'background-color: {}'.format('#FF6347'))

        # validation check for readings < CONC
        df1[deg0_reading_cols[i]] = np.where(TML_df[deg0_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                             'background-color: {}'.format('#FF6347'), df1[deg0_reading_cols[i]])
        df1[deg90_reading_cols[i]] = np.where(TML_df[deg90_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                              'background-color: {}'.format('#FF6347'), df1[deg90_reading_cols[i]])
        df1[deg180_reading_cols[i]] = np.where(TML_df[deg180_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FF6347'), df1[deg180_reading_cols[i]])
        df1[deg270_reading_cols[i]] = np.where(TML_df[deg270_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FF6347'), df1[deg270_reading_cols[i]])
        df1[min_WT_reading_cols[i]] = np.where(TML_df[min_WT_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FF6347'), df1[min_WT_reading_cols[i]])
        df1[max_WT_reading_cols[i]] = np.where(TML_df[max_WT_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FF6347'), df1[max_WT_reading_cols[i]])
        df1[avg_WT_reading_cols[i]] = np.where(TML_df[avg_WT_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FF6347'), df1[avg_WT_reading_cols[i]])

        # validation check for  consecutive year's thickness readings
        df1[deg0_reading_cols[i]] = np.where(TML_df[deg0_reading_cols[i]] - TML_df[deg0_reading_cols[i - 1]] > 0.5,
                                             'background-color: {}'.format('#FF6347'), df1[deg0_reading_cols[i]])
        df1[deg90_reading_cols[i]] = np.where(TML_df[deg90_reading_cols[i]] - TML_df[deg90_reading_cols[i - 1]] > 0.5,
                                              'background-color: {}'.format('#FF6347'), df1[deg90_reading_cols[i]])
        df1[deg180_reading_cols[i]] = np.where(
            TML_df[deg180_reading_cols[i]] - TML_df[deg180_reading_cols[i - 1]] > 0.5,
            'background-color: {}'.format('#FF6347'), df1[deg180_reading_cols[i]])
        df1[deg270_reading_cols[i]] = np.where(
            TML_df[deg270_reading_cols[i]] - TML_df[deg270_reading_cols[i - 1]] > 0.5,
            'background-color: {}'.format('#FF6347'), df1[deg270_reading_cols[i]])
        df1[min_WT_reading_cols[i]] = np.where(
            TML_df[min_WT_reading_cols[i]] - TML_df[min_WT_reading_cols[i - 1]] > 0.5,
            'background-color: {}'.format('#FF6347'), df1[min_WT_reading_cols[i]])
        df1[max_WT_reading_cols[i]] = np.where(
            TML_df[max_WT_reading_cols[i]] - TML_df[max_WT_reading_cols[i - 1]] > 0.5,
            'background-color: {}'.format('#FF6347'), df1[max_WT_reading_cols[i]])
        df1[avg_WT_reading_cols[i]] = np.where(
            TML_df[avg_WT_reading_cols[i]] - TML_df[avg_WT_reading_cols[i - 1]] > 0.5,
            'background-color: {}'.format('#FF6347'), df1[avg_WT_reading_cols[i]])

        # validation check for readings == CONC
        df1[deg0_reading_cols[i]] = np.where(TML_df[deg0_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                             'background-color: {}'.format('#FFBF00'), df1[deg0_reading_cols[i]])
        df1[deg90_reading_cols[i]] = np.where(TML_df[deg90_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                              'background-color: {}'.format('#FFBF00'), df1[deg90_reading_cols[i]])
        df1[deg180_reading_cols[i]] = np.where(TML_df[deg180_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FFBF00'), df1[deg180_reading_cols[i]])
        df1[deg270_reading_cols[i]] = np.where(TML_df[deg270_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FFBF00'), df1[deg270_reading_cols[i]])
        df1[min_WT_reading_cols[i]] = np.where(TML_df[min_WT_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FFBF00'), df1[min_WT_reading_cols[i]])
        df1[max_WT_reading_cols[i]] = np.where(TML_df[max_WT_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FFBF00'), df1[max_WT_reading_cols[i]])
        df1[avg_WT_reading_cols[i]] = np.where(TML_df[avg_WT_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                               'background-color: {}'.format('#FFBF00'), df1[avg_WT_reading_cols[i]])

    # df1['D'] = np.where(m2, 'background-color: {}'.format(g), df1['D'])
    return df1

for subdir, dirs, files in os.walk(rootdir):
    for file in files:
        if file.endswith('_for_QC.xlsx'):
            if file.endswith('validated.xlsx') == False:  # to avoid reading the filled file again
                print("\nValidating and generating circuit TML: ", file)
                TML_to_validate = pd.read_excel(os.path.join(subdir, file), header=2)
                #TML_to_validate_multiIndex = pd.read_excel(os.path.join(subdir, file), header=[0,1,2])
                #TML_to_validate = pd.read_excel('DNO_inspection_reports/test/TML_to_validate.xlsx')
                # display(TML_df_QCed.columns, TML_df_to_fill.columns)
                for col in TML_to_validate.columns:
                    try:
                        TML_to_validate[col] = TML_to_validate[col].astype(float)
                        #TML_to_validate[col] = pd.to_numeric(TML_to_validate[col], errors='coerce')

                        #TML_to_validate[col] = TML_to_validate[col].astype(float)
                    except:
                        pass
                TML_df = TML_to_validate.copy()
                #TML_df = TML_df.apply(pd.to_numeric, errors='coerce')

                TML_df = TML_df.replace('NaT', np.NaN, regex=True)
                TML_df = TML_df.replace('nan', np.NaN, regex=True)
                TML_df = TML_df.replace(' ', np.NaN, regex=True)
                # TML_df = TML_df.apply(lambda x: x.str.strip() if isinstance(x, str) else x).replace('', np.nan)

                #TML_df['NWT, mm'] = TML_df['NWT, mm'].str.extract('(\d+)').astype(float)

                # TML_df.to_excel('DNO_inspection_reports/test/TML_filled_with_readings.xlsx', index=False)
                deg0_reading_cols = [col for col in TML_df.columns if 'Reading @ 0 deg' in col]
                deg90_reading_cols = [col for col in TML_df.columns if 'Reading @ 90 deg' in col]
                deg180_reading_cols = [col for col in TML_df.columns if 'Reading @ 180 deg' in col]
                deg270_reading_cols = [col for col in TML_df.columns if 'Reading @ 270 deg' in col]

                # display(deg0_reading_cols)
                min_WT_reading_cols = [col for col in TML_df.columns if 'Minimum' in col]
                max_WT_reading_cols = [col for col in TML_df.columns if 'Maximum' in col]
                avg_WT_reading_cols = [col for col in TML_df.columns if 'Average' in col]

                comment_cols = [col for col in TML_df.columns if 'Comment' in col]

                report_num_cols = [col for col in TML_df.columns if 'Report' in col]
                takenby_cols = [col for col in TML_df.columns if 'Taken' in col]
                probeid_cols = [col for col in TML_df.columns if 'Probe' in col]

                # for i in range(len(comment_cols)):
                #  TML_df[comment_cols[i]] = TML_df[comment_cols[i]].astype(str)

                #TML_df['NWT, mm'] = TML_df[TML_df['NWT, mm'].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                #TML_df['MAWT, mm'] = TML_df[TML_df['MAWT, mm'].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                #TML_df['CONC, mm'] = TML_df[TML_df['CONC, mm'].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                TML_df['NWT, mm'] = pd.to_numeric(TML_df['NWT, mm'], errors='coerce')
                TML_df['MAWT, mm'] = pd.to_numeric(TML_df['MAWT, mm'], errors='coerce')
                TML_df['CONC, mm'] = pd.to_numeric(TML_df['CONC, mm'], errors='coerce')
                for col in min_WT_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')
                    TML_df[col] = TML_df[col].astype(float)
                for col in max_WT_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')
                    TML_df[col] = TML_df[col].astype(float)

                for col in avg_WT_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')
                    TML_df[col] = TML_df[col].astype(float)

                for col in deg0_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')

                for col in deg90_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')

                for col in deg180_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')

                for col in deg270_reading_cols:
                    #TML_df[col] = TML_df[TML_df[col].apply(lambda x: type(x) in [int, np.int64, float, np.float64])]
                    TML_df[col] = pd.to_numeric(TML_df[col], errors='coerce')

                for i in range(len(comment_cols)):  # This can be any parameter as all columns are of same length
                    # Comments filling for  NWT readings
                    TML_df[comment_cols[i]] = np.where((TML_df[deg0_reading_cols[i]].isna() | TML_df[
                        deg90_reading_cols[i]].isna() | TML_df[deg180_reading_cols[i]].isna() | TML_df[
                                                            deg270_reading_cols[i]].isna() | TML_df[
                                                            min_WT_reading_cols[i]].isna() | TML_df[
                                                            max_WT_reading_cols[i]].isna() | TML_df[
                                                            avg_WT_reading_cols[i]].isna() | TML_df['NWT, mm'].isna() |
                                                        TML_df['CONC, mm'].isna()), '', TML_df[comment_cols[i]])
                    TML_df[comment_cols[i]] = np.where((TML_df[deg0_reading_cols[i]] > (TML_df['NWT, mm'] * 0.875)) & (TML_df[deg0_reading_cols[i]] < (TML_df['NWT, mm'] * 1.125)) & (TML_df[deg0_reading_cols[i]] > TML_df['CONC, mm'].isna()),TML_df[comment_cols[i]], TML_df[comment_cols[i]] + "Reading @ 0 deg is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[deg0_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    TML_df[comment_cols[i]] = np.where((TML_df[deg90_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                                TML_df[deg90_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                                   TML_df[deg90_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                                       TML_df[comment_cols[i]], TML_df[
                                                           comment_cols[i]] + "Reading @ 90 deg is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[deg90_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    TML_df[comment_cols[i]] = np.where((TML_df[deg180_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                                TML_df[deg180_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                                   TML_df[deg180_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                                       TML_df[comment_cols[i]], TML_df[
                                                           comment_cols[i]] + "Reading @ 180 deg is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[deg180_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    TML_df[comment_cols[i]] = np.where((TML_df[deg270_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                                TML_df[deg270_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                                   TML_df[deg270_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                                       TML_df[comment_cols[i]], TML_df[
                                                           comment_cols[i]] + "Reading @ 270 deg is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[deg270_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    TML_df[comment_cols[i]] = np.where((TML_df[min_WT_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                                TML_df[min_WT_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                                   TML_df[min_WT_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                                       TML_df[comment_cols[i]],
                                                       TML_df[comment_cols[i]] + "Minimum WT is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[min_WT_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    TML_df[comment_cols[i]] = np.where((TML_df[max_WT_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                                TML_df[max_WT_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                                   TML_df[max_WT_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                                       TML_df[comment_cols[i]],
                                                       TML_df[comment_cols[i]] + "Maximum WT is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[max_WT_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    TML_df[comment_cols[i]] = np.where((TML_df[avg_WT_reading_cols[i]] > TML_df['NWT, mm'] * 0.875) & (
                                TML_df[avg_WT_reading_cols[i]] < TML_df['NWT, mm'] * 1.125) & (
                                                                   TML_df[avg_WT_reading_cols[i]] > TML_df['CONC, mm'].isna()),
                                                       TML_df[comment_cols[i]],
                                                       TML_df[comment_cols[i]] + "Average WT is beyond 12.5% of NWT \n")
                    TML_df[comment_cols[i]] = np.where(
                        (TML_df[avg_WT_reading_cols[i]].isna()), '',
                        TML_df[comment_cols[i]])

                    # Comment filling for readings < CONC
                    TML_df[comment_cols[i]] = np.where(TML_df[deg0_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 0 deg is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg0_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[deg90_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 90 deg is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg90_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[deg180_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 180 deg is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg180_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[deg270_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 270 deg is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg270_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[min_WT_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Minimum WT is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[min_WT_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[max_WT_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Maximum WT is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[max_WT_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[avg_WT_reading_cols[i]] < TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Average WT is less than CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[avg_WT_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    # Comment filling for consecutive year's thickness readings
                    TML_df[comment_cols[i]] = np.where(
                        TML_df[deg0_reading_cols[i]] - TML_df[deg0_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Reading @ 0 deg increased over 0.5mm from previous year \n ",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg0_reading_cols[i]].isna() | TML_df[deg0_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(
                        TML_df[deg90_reading_cols[i]] - TML_df[deg90_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Reading @ 90 deg increased over 0.5mm from previous year \n",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg90_reading_cols[i]].isna() | TML_df[deg90_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(
                        TML_df[deg180_reading_cols[i]] - TML_df[deg180_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Reading @ 180 deg increased over 0.5mm from previous year \n",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg180_reading_cols[i]].isna() | TML_df[deg180_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(
                        TML_df[deg270_reading_cols[i]] - TML_df[deg270_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Reading @ 270 deg increased over 0.5mm from previous year \n",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg270_reading_cols[i]].isna() | TML_df[deg270_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(
                        TML_df[min_WT_reading_cols[i]] - TML_df[min_WT_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Minimum WT has increased over 0.5mm from previous year \n",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[min_WT_reading_cols[i]].isna() | TML_df[min_WT_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(
                        TML_df[max_WT_reading_cols[i]] - TML_df[max_WT_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Maximum WT has increased over 0.5mm from previous year \n",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[max_WT_reading_cols[i]].isna() | TML_df[max_WT_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(
                        TML_df[avg_WT_reading_cols[i]] - TML_df[avg_WT_reading_cols[i - 1]] > 0.5,
                        TML_df[comment_cols[i]] + "Average WT has increased over 0.5mm from previous year \n",
                        TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[avg_WT_reading_cols[i]].isna() | TML_df[avg_WT_reading_cols[i-1]].isna()), TML_df[comment_cols[i]], '' )

                    # Comment filling for readings == CONC
                    TML_df[comment_cols[i]] = np.where(TML_df[deg0_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 0 deg is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg0_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[deg90_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 90 deg is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg90_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[deg180_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 180 deg is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg180_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[deg270_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Reading @ 270 deg is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[deg270_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[min_WT_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Minimum WT is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[min_WT_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[max_WT_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Maximum WT is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[max_WT_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                    TML_df[comment_cols[i]] = np.where(TML_df[avg_WT_reading_cols[i]] == TML_df['CONC, mm'].isna(),
                                                       TML_df[comment_cols[i]] + "Average WT is equal to CONC \n",
                                                       TML_df[comment_cols[i]])
                    # TML_df[comment_cols[i]] = np.where(  (TML_df[avg_WT_reading_cols[i]].isna() | TML_df['CONC, mm'].isna()), TML_df[comment_cols[i]], '' )

                TML_df = TML_df.replace('nan', '', regex=True)


                TML_df['Feature Type'] = TML_to_validate['Feature Type']
                TML_df['Feature ID'] = TML_to_validate['Feature ID']

                for col in comment_cols:
                    TML_df[col] = np.where(TML_to_validate[col].isna(), TML_df[col],
                                           TML_to_validate[col] + "\n" + TML_df[col])

                for col in report_num_cols:
                    TML_df[col] = TML_to_validate[col]
                for col in probeid_cols:
                    TML_df[col] = TML_to_validate[col]
                for col in takenby_cols:
                    TML_df[col] = TML_to_validate[col]

                #TML_df.columns = TML_to_validate_multiIndex.columns
                #TML_df = TML_df.set_index([('Circuit Tag', 'Automatically filled from piping loadsheet','Piping Tag/ Line Number*')]).rename_axis(index=None,columns=('Circuit Tag','Automatically filled from piping loadsheet','Piping Tag/ Line Number*'))
                #TML_df = TML_df.style.apply(lambda x: ['background: yellow' if x.name in range(len(df_fill_unmatched_entries), len(df_concatenated_in_loop_new),-1) else '' for i in x], axis=1)
                #TML_df.style.apply(highlight, axis=None).highlight_null('None').apply(lambda x: ['background: yellow' if x.name in list(range(len(TML_df), len(TML_df)-len(df_fill_unmatched_entries),-1)) else '' for i in x], axis=1).to_excel(os.path.join(subdir, os.path.splitext(file)[0])+'_validated.xlsx', engine='xlsxwriter', index=False)  # coloring based on index


                #TML_df.style.apply(highlight, axis=None).highlight_null('None').apply(lambda x: ['background-color: yellow' if x.name in list(range(len(TML_df), len(TML_df) - len(df_fill_unmatched_entries)-1, -1)) else '' for i in x],axis=1, subset=['Piping Tag/ Line Number*',	'Feature Type',	'Feature ID']).to_excel(os.path.join(subdir, os.path.splitext(file)[0]) + '_validated.xlsx', engine='xlsxwriter',index=False)  # coloring based on index
                TML_df.style.apply(highlight, axis=None).highlight_null('None').apply(lambda x: [
                    'background-color: yellow' if x.name in list(
                        range(len(TML_df), len(TML_df) - len(df_fill_unmatched_entries) - 1, -1)) else '' for i in x],
                                                                                      axis=1, subset=[
                        'Piping Tag/ Line Number*', 'Feature Type', 'Feature ID']).to_excel(
                    os.path.join(subdir,file), engine='xlsxwriter',
                    index=False)  # coloring based on index


end_time = time.time()
print(
    "Data extraction and validation of Inspection Reports complete. \n Filled-in thickness loadsheets are saved as [circuit_name]_for_QC.xlsx under each circuit folder\n Perform QA/QC on the generated loadsheets\n\n Window closes in 20 seconds\n")


def countdown(t):
    while t:
        mins, secs = divmod(t, 60)
        time.sleep(1)
        timeformat = '{:02d}:{:02d}'.format(mins, secs)
        print(timeformat + 's  ---->', end="\t", flush=True)  # , end='\r')
        # sys.stdout.flush()
        t -= 1
    # print('Goodbye!\n\n\n\n\n')


# print(df_concatenated_in_loop_excel_JV)
countdown(20)
######################################################
#######################################################
######################################################
